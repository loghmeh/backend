CREATE DATABASE IF NOT EXISTS loghmeh;

ALTER DATABASE loghmeh
    DEFAULT CHARACTER SET utf8
    DEFAULT COLLATE utf8_general_ci;

USE loghmeh;

CREATE TABLE IF NOT EXISTS restaurants(
    id VARCHAR(63) NOT NULL UNIQUE PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    loc_x FLOAT,
    loc_y FLOAT,
    logo VARCHAR(2083)
    -- INDEX USING BTREE (name),
)engine=InnoDB;

CREATE TABLE IF NOT EXISTS users(
    id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    loc_x FLOAT,
    loc_y FLOAT,
    email VARCHAR(255) UNIQUE NOT NULL,
    phone_number VARCHAR(30) NOT NULL,
    balance INT(4)
)engine=InnoDB;

CREATE TABLE IF NOT EXISTS carts(
    user_id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    rest_id VARCHAR(63),
    cart_mode INT(4),
    FOREIGN KEY (rest_id)
        REFERENCES restaurants(id),
    FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
)engine=InnoDB;

CREATE TABLE IF NOT EXISTS orders(
    id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    rest_id VARCHAR(63),
    status INT(4),
    eta VARCHAR(30),
    user_id INT(4) UNSIGNED,
    FOREIGN KEY (rest_id)
        REFERENCES restaurants(id),
    FOREIGN KEY (user_id)
        REFERENCES users(id)
)engine=InnoDB;

CREATE TABLE IF NOT EXISTS foods(
    id VARCHAR(63) NOT NULL UNIQUE PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    image VARCHAR(2083),
    description VARCHAR(250),
    popularity float,
    price int,
    rest_id VARCHAR(63),
    FOREIGN KEY(rest_id)
        REFERENCES restaurants(id)
        ON DELETE CASCADE
    -- INDEX USING BTREE (name),
)engine=InnoDB;


# TODO: Beloww table doesnt have any PKs!
CREATE TABLE IF NOT EXISTS cart_items(
    count int UNSIGNED NOT NULL,
    food_id VARCHAR(63),
    cart_id INT(4) UNSIGNED,
    order_id INT(4) UNSIGNED,
    FOREIGN KEY(food_id)
        REFERENCES foods(id),
    FOREIGN KEY(cart_id)
        REFERENCES carts(user_id)
        ON DELETE CASCADE,
    FOREIGN KEY(order_id)
        REFERENCES orders(id)
        ON DELETE CASCADE
)engine=InnoDB;

CREATE TABLE IF NOT EXISTS party_foods(
   count int NOT NULL,
   old_price int,
   status BOOLEAN DEFAULT 1,
   food_id VARCHAR(63),
   FOREIGN KEY(food_id)
        REFERENCES foods(id)
        ON DELETE CASCADE
)engine=InnoDB;

CREATE INDEX ind_name
ON foods (name);

CREATE INDEX ind_name
ON restaurants (name);