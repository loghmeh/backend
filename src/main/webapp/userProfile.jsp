<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="user" value="${requestScope.user}"/>
<html>
<head>
    <title>لقمه!</title>
    <link href="<c:url value="sources/navbar.css"/>" rel="stylesheet">
</head>
<body>
<ul class="nav">
<%--    <li class="nav"><a href="">خانه</a></li>--%>
    <li class="nav"><a href="restaurants">رستوران‌ها</a></li>
    <li class="nav"><a href="foodParty">فود پارتی</a></li>
    <li class="nav"><a href="cart">سبد خرید</a></li>
    <li class="nav"><a class="active" href="profile">پروفایل</a></li>

    <li style="float:right" class="nav userinfo">!${user.firstName} سلام</li>
    <li style="float:right" class="nav userinfo">اعتبار: ${user.balance} تومان</li>
</ul>

<c:if test="${!empty requestScope.message}">
    <p>${requestScope.message}</p>
</c:if>

<ul>
    <li>full name: ${user.firstName} ${user.lastName}</li>
    <li>phone number: ${user.phoneNumber}</li>
    <li>email: ${user.email}</li>
    <li>credit: ${user.balance} Toman
        <form action="addBalance" method="POST">
            <button type="submit">increase</button>
            <input type="number" name="amount" value="0"/>
        </form>
    </li>
    <li>
        Orders:

        <ul>
            <c:forEach items="${user.orders}" var="order">
            <li>
<%--                TODO: Implement view order details--%>
                <a href="orderDetails?id=${order.id}">order id: ${order.id}</a>
            </li>
            </c:forEach>
        </ul>
    </li>
</ul>
</body>
</html>
