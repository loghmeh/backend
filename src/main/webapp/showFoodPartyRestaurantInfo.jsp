<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="restaurant" value="${requestScope.restaurant}"/>


<html>
<head>
    <title>لقمه!</title>
</head>
<body>
<p>${requestScope.message}</p>

<c:if test="${!empty requestScope.restaurant}">
    <ul>
        <li>id: ${restaurant.id}</li>
        <li>name: ${restaurant.name}</li>
        <li>location: (${restaurant.location.x}, ${restaurant.location.x})</li>
        <li>logo: <img class="logo" src="${restaurant.logo}" alt="restaurant-logo"></li>
        <li>زمان تقریبی رسیدن غذا: ${requestScope.estimatedEta}</li>
        <li>منوی فود پارتی:
            <ul>
                <c:forEach items="${restaurant.partyMenu}" var="menuItem">
                    <c:set var="food" value="${menuItem.value}"/>
                    <li>
                        <img class="logo" src="${food.image}" alt="food-logo">
                        <div>${food.name}</div>
                        <div>قیمت قدیم: ${food.oldPrice}</div>
                        <div>قیمت فود پارتی: ${food.price} Toman</div>
                        <div>تعداد باقی‌مانده: ${food.count}</div>
                        <form action="addPartyFoodToCart" method="POST">
                            <input hidden name="foodName" value="${food.name}">
                            <input hidden name="restaurantId" value="${restaurant.id}">
                            <button type="submit">اضافه به سبد خرید</button>
                        </form>
                    </li>
                </c:forEach>
            </ul>
        </li>
    </ul>
</c:if>

</body>
</html>
