<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="restaurant" value="${requestScope.restaurant}"/>
<html>
<head>
    <title>لقمه!</title>
</head>
<body>
<p>${requestScope.message}</p>

<c:if test="${!empty requestScope.restaurant}">
<ul>
    <li>id: ${restaurant.id}</li>
    <li>name: ${restaurant.name}</li>
    <li>location: (${restaurant.location.x}, ${restaurant.location.x})</li>
    <li>logo: <img class="logo" src="${restaurant.logo}" alt="restaurant-logo"></li>
    <li>زمان تقریبی رسیدن غذا: ${requestScope.estimatedEta}</li>

    <li>Menu:
        <ul>
            <c:forEach items="${restaurant.menu}" var="menuItem">
                <c:set var="food" value="${menuItem.value}"/>
                <li>
                    <img class="logo" src="${food.image}" alt="food-logo">
                    <div>${food.name}</div>
                    <div>${food.price} Toman</div>
                    <form action="addToCart" method="POST">
                        <input hidden name="foodName" value="${food.name}">
                        <input hidden name="restaurantId" value="${restaurant.id}">
                        <button type="submit">addToCart</button>
                    </form>
                </li>
            </c:forEach>
        </ul>
    </li>
</ul>
</c:if>

</body>
</html>
