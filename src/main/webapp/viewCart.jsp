<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="foods" value="${requestScope.foods}"/>
<html>
<head>
    <title>لقمه!</title>
</head>
<body>

<c:if test="${!empty requestScope.message}">
    <p>${requestScope.message}</p>
</c:if>

<c:if test="${!empty foods}">
    <p>
        ${requestScope.restaurantName}
    </p>
    <ul>
        <c:forEach items="${foods}" var="foodCartItem">
            <li>
                <div>${foodCartItem.food.name}</div>
                <div>${foodCartItem.food.price} تومان </div>
                <div>${foodCartItem.count} عدد </div>
            </li>
        </c:forEach>
        <li>
            <p>مجموع: ${requestScope.totalPrice}</p>
        </li>
    </ul>
    <form action="finalizeOrder" method="POST">
        <button type="submit">نهایی کردن سفارش</button>
    </form>
</c:if>

</body>
</html>
