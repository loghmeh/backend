<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="order" value="${requestScope.order}"/>
<c:set var="restaurant" value="${requestScope.restaurant}"/>
<c:set var="deliveryTime" value="${requestScope.deliveryTime}"/>

<html>
<head>
    <title>لقمه!</title>
</head>
<body>
<p>${requestScope.message}</p>

<c:if test="${!empty order}">
    <ul>
        <li>شناسه‌ی سفارش: ${order.id}</li>
        <li>رستوران: ${restaurant.name}</li>
        <li>وضعیت سفارش: ${order.status}</li>
    <c:if test="${!empty deliveryTime}">
        <li>تا تحویل: <p id="timer"></p></li>
    </c:if>
        <li>logo: <img class="logo" src="${restaurant.logo}" alt="restaurant-logo"></li>
        <li>Foods:
            <ul>
                <c:forEach items="${order.foods}" var="cartFoodItem">
                    <c:set var="food" value="${cartFoodItem.food}"/>
                    <li>
                        <div>غذا: ${food.name}</div>
                        <div><img src="${food.image}" alt="food-image"></div>
                        <div>قیمت: ${food.price}</div>
                        <div>تعداد: ${cartFoodItem.count}</div>
                    </li>
                </c:forEach>
            </ul>
        </li>
        <li>هزینه‌ی کل: ${order.totalPrice}</li>
    </ul>
</c:if>

<c:if test="${!empty deliveryTime}">
    <script>
        var countDownDate = new Date("${deliveryTime}").getTime();
        var x = setInterval(function() {
            var now = new Date().getTime();
            var distance = countDownDate - now;
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            document.getElementById("timer").innerHTML = days + "d " + hours + "h "
                + minutes + "m " + seconds + "s ";
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("timer").innerHTML = "نوش جان!";
            }
        }, 1000);
    </script>
</c:if>
</body>
</html>
