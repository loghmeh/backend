<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="restaurant" value="${requestScope.restaurant}"/>
<c:set var="restaurantInfoReferenceEndpoint" value="${empty requestScope.foodParty ? 'restaurantInfo': 'partyRestaurantInfo'}"/>

<html>
<head>
    <title>لقمه!</title>
</head>
<body>
    <p>${requestScope.message}</p>

<c:if test="${!empty requestScope.restaurants}">
    <table>
        <tr>
            <th>id</th>
            <th>logo</th>
            <th>name</th>
            <th>location</th>
        </tr>

        <c:forEach items="${requestScope.restaurants}" var="restaurant">
                <tr>
                    <td><a href="${restaurantInfoReferenceEndpoint}?id=${restaurant.id}">${restaurant.id}</a></td>
                    <td><img class="logo" src="${restaurant.logo}" alt="restaurant-logo"></td>
                    <td>${restaurant.name}</td>
                    <td>(${restaurant.location.x}, ${restaurant.location.x})</td>
                </tr>
        </c:forEach>
    </table>
</c:if>
</body>
</html>
