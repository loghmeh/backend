<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="restaurantInfoReferenceEndpoint" value="${empty requestScope.foodParty ? 'restaurantInfo': 'partyRestaurantInfo'}"/>

<html>
<head>
    <title>لقمه!</title>
</head>
<body>
<p>${requestScope.message}</p>

<c:if test="${!empty requestScope.restaurantReference}">
    <dev>
        <a href="${restaurantInfoReferenceEndpoint}?id=${requestScope.restaurantReference}">بازگشت به رستوران</a>
    </dev>
</c:if>

</body>
</html>
