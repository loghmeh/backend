package Config;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class Loghmeh {
    public static final String DB_HOST = getEnv("DB_HOST", "localhost");
    public static final String DB_PORT = getEnv("DB_PORT", "3306");
    public static final String DB_NAME = getEnv("DB_NAME", "loghmeh");
    public static final String DB_USER = getEnv("DB_USER", "root");
    public static final String DB_PASS = getEnv("DB_PASS", "loghiloghi");
    public static final String REDIS_HOST = getEnv("REDIS_HOST", "localhost");
    public static final String REDIS_PORT = getEnv("REDIS_PORT", "6379");
    public static final SecretKey PRIVATE_KEY = Keys.hmacShaKeyFor(
            Base64.getDecoder().decode("i8eW6i1he0Np0d92TGcuek5E7LRlHINTDtYjeXLzVUv+ETC8gXypWo/dw7jGhm2u")
    );
    public static final String AUTH_HEADER_KEY = "Authorization";
    public static final String AUTH_HEADER_VALUE_PREFIX = "JWT";
    public static final String GOOGLE_AUTH_CLIENT_ID = "38170085199-b2r48ppgthlt3b3oe3enojl65ab3bqbp.apps.googleusercontent.com";

    private static String getEnv(String envName, String defaultValue) {
        String env = System.getenv(envName);
        return env == null ? defaultValue : env;
    }
}
