//package Controller;
//import BusinessLogic.*;
//import Commands.Response.StatusResponse;
//import Data.Access.DatabaseAccess;
//import Data.Access.UserSessionAccess;
//import Data.CartFoodItem;
//import Data.Food;
//import Data.Restaurant;
//import Data.User;
//import Util.Path;
//import io.javalin.http.Handler;
//
//import java.util.*;
//
//public class UserController {
//    public static Handler addFoodToCard = ctx -> {
//        String restId = ctx.formParam("restId");
//        String foodName = ctx.formParam("foodName");
//        Command addFood = new AddFoodToCartHandler(restId, foodName);
//        StatusResponse addFoodResponse = (StatusResponse)addFood.execute(currentUser);
//        ctx.status(addFoodResponse.getStatus());
//        Restaurant restaurant = DatabaseAccess.getRestaurantById(restId);
//        Map<String, Object> model = new HashMap<>();
//        model.put("rest", restaurant);
//        HashMap<String, Food> menu = restaurant.getMenu();
//        List<Food> foods = new LinkedList<>(menu.values());
//        model.put("foods", foods);
//        ctx.render(Path.Template.REST_ONE, model);
//    };
//    public static Handler fetchCredits = ctx -> {
//        User user = UserSessionAccess.getUserInfo();
//        String id = "1";
//        Map<String, Object> model = new HashMap<>();
//        model.put("user", user);
//        model.put("id", id);
//        ctx.render(Path.Template.USER, model);
//
//
//    };
//    public static Handler fetchCart = ctx -> {
//        Command getCart = new GetCartCommand();
//        Object response = getCart.execute(currentUser);
//        Map<String, Object> model = new HashMap<>();
//        if(response instanceof  StatusResponse ){
//            StatusResponse statusResponse = (StatusResponse) response;
//            ctx.status(statusResponse.getStatus());
//        }
//        else {
//            ArrayList<CartFoodItem> foodList = (ArrayList<CartFoodItem>) getCart.execute(currentUser);
//            model.put("cartfoods", foodList);
//        }
//        ctx.render(Path.Template.CART, model);
//
//    };
//    public static Handler charge = ctx -> {
//        int amount = Integer.parseInt(ctx.formParam("credit"));
//        Command charge = new ChargeAccountCommand(amount);
//        Object response = charge.execute(currentUser);
//        StatusResponse statusResponse = (StatusResponse)response;
//        ctx.status(statusResponse.getStatus());
//        User user = UserSessionAccess.getUserInfo();
//        String id = "1";
//        Map<String, Object> model = new HashMap<>();
//        model.put("user", user);
//        model.put("id", id);
//        ctx.render(Path.Template.USER, model);
//    };
//    public static Handler checkout = ctx -> {
//        Command finalize = new FinalizeOrderCommand();
//        finalize.execute(currentUser);
//        User user = UserSessionAccess.getUserInfo();
//        String id = "1";
//        Map<String, Object> model = new HashMap<>();
//        model.put("user", user);
//        model.put("id", id);
//        ctx.render(Path.Template.USER, model);
//    }
//    ;
//}
