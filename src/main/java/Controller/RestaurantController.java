//package Controller;
//
//import BusinessLogic.Command;
//import Commands.Response.StatusResponse;
//import BusinessLogic.ViewAvailableRestaurantsCommand;
//import BusinessLogic.ViewRestaurantInfoCommand;
//import Data.Food;
//import Data.Restaurant;
//import Util.Path;
//import io.javalin.http.Handler;
//
//import java.util.*;
//
//public class RestaurantController {
//    public static Handler fetchAllRestaurants = ctx -> {
//        ViewAvailableRestaurantsCommand restListCommand = new ViewAvailableRestaurantsCommand();
//        Object response = restListCommand.execute(currentUser);
//        if( response instanceof StatusResponse ){
//            StatusResponse statusResponse = (StatusResponse) response;
//            ctx.status(statusResponse.getStatus());
//        }
//        else{
//            ArrayList<Restaurant> restList = (ArrayList<Restaurant>) response;
//            Map<String, Object> model = new HashMap<>();
//            model.put("rests", restList);
//            ctx.render(Path.Template.REST_ALL, model);
//        }
//    };
//    public static Handler fetchRestaurant = ctx -> {
//        String restId = ctx.pathParam("id");
//        Command restInfo = new ViewRestaurantInfoCommand(restId);
//        Object response = restInfo.execute(currentUser);
//        if(response instanceof  StatusResponse){
//            StatusResponse statusResponse = (StatusResponse) response;
//            ctx.status(statusResponse.getStatus());
//        }
//        else {
//            Restaurant restaurant;
//            restaurant = (Restaurant)response;
//            Map<String, Object> model = new HashMap<>();
//            model.put("rest", restaurant);
//            HashMap<String, Food> menu = restaurant.getMenu();
//            List<Food> foods = new LinkedList<>(menu.values());
//            model.put("foods", foods);
//            ctx.render(Path.Template.REST_ONE, model);
//        }
//    };
//}
