package DTO;

import Model.Cart;
import Model.CartFoodItem;
import Model.CartMode;

import java.util.ArrayList;

public class CartDTO {
    private ArrayList<CartFoodItem> foods;
    private int totalPrice;
    private String restaurantId;
    private CartMode cartMode;

    public CartDTO() {
        this.foods = new ArrayList<>();
        this.totalPrice = 0;
        this.restaurantId = null;
        this.cartMode = null;
    }

    public CartDTO(Cart cart) {
        foods = cart.getFoods();
        totalPrice = cart.getTotalPrice();
        restaurantId = cart.getRestaurantId();
        cartMode = cart.getCartMode();
    }

    public ArrayList<CartFoodItem> getFoods() {
        return foods;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public CartMode getCartMode() {
        return cartMode;
    }
}
