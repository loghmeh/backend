package DTO;

import Model.Restaurant;

import java.util.ArrayList;

public class RestaurantListDTO {
    private ArrayList<Restaurant> restaurants;

    public RestaurantListDTO(ArrayList<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }

    public ArrayList<Restaurant> getRestaurants() {
        return restaurants;
    }
}
