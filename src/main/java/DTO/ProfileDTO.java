package DTO;

import Model.Cart;
import Model.Order;
import Model.User;

import java.util.ArrayList;

public class ProfileDTO {
    private Cart cart;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private Integer balance;
    private ArrayList<OrderDTO> orders;

    public ProfileDTO(User user) {
        cart = user.getCart();
        firstName = user.getFirstName();
        lastName = user.getLastName();
        email = user.getEmail();
        phoneNumber = user.getPhoneNumber();
        balance = user.getBalance();
        orders = new ArrayList<>();
        for (Order order: user.getOrders()) {
            orders.add(new OrderDTO(order));
        }
    }

    public Cart getCart() {
        return cart;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Integer getBalance() {
        return balance;
    }

    public ArrayList<OrderDTO> getOrders() {
        return orders;
    }
}
