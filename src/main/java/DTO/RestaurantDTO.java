package DTO;

import Model.Food;
import Model.Location;
import Model.PartyFood;
import Model.Restaurant;

import java.net.URL;
import java.util.ArrayList;

public class RestaurantDTO {
    private String id;
    private String name;
    private Location location;
    private URL logo;
    private ArrayList<Food> menu;
    private ArrayList<PartyFood> partyMenu;;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public URL getLogo() {
        return logo;
    }

    public void setLogo(URL logo) {
        this.logo = logo;
    }



    public String getName() {
        return name;
    }


    public Location getLocation() {
        return location;
    }

    public ArrayList<Food> getMenu() {
        return menu;
    }

    public ArrayList<PartyFood> getPartyMenu() {
        return partyMenu;
    }

    public RestaurantDTO(Restaurant restaurant) {
        this.id = restaurant.getId();
        this.name = restaurant.getName();
        this.logo = restaurant.getLogo();
        this.location = restaurant.getLocation();
        this.menu = new ArrayList<>(restaurant.getMenu());
        this.partyMenu = new ArrayList<>(restaurant.getPartyMenu());
    }

    @Override
    public String toString() {
        return "RestaurantDTO{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", location=" + location +
                ", logo=" + logo +
                ", menu=" + menu +
                ", partyMenu=" + partyMenu +
                '}';
    }
}
