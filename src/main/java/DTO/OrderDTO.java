package DTO;

import Data.Access.DatabaseAccess;
import Data.Manager.RestaurantManager;
import Exceptions.RestaurantDoesntExistException;
import Model.CartFoodItem;
import Model.Order;
import Model.OrderStatus;
import Model.Restaurant;

import java.util.ArrayList;
import java.util.Objects;

public class OrderDTO {
    private ArrayList<CartFoodItem> foods;
    private RestaurantMetadataDTO restaurant;
    private int id;
    private long totalPrice;
    private OrderStatus status;
    private String eta;

    public OrderDTO(Order order) {
        //TODO: this smells a lot
        try {
            restaurant = new RestaurantMetadataDTO(
                    RestaurantManager.getRestaurantById(order.getRestaurantId())
            );
        } catch (RestaurantDoesntExistException e) {
            // ignore
        }
        foods = order.getFoods();
        id = order.getId();
        totalPrice = order.getTotalPrice();
        status = order.getStatus();
        eta = order.getEta();
    }

    public ArrayList<CartFoodItem> getFoods() {
        return foods;
    }

    public RestaurantMetadataDTO getRestaurant() {
        return restaurant;
    }

    public int getId() {
        return id;
    }

    public long getTotalPrice() {
        return totalPrice;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public String getEta() {
        return eta;
    }
}
