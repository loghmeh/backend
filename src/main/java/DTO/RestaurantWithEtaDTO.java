package DTO;

import Model.Restaurant;

public class RestaurantWithEtaDTO {
    private RestaurantDTO restaurant;
    private String eta;

    public RestaurantWithEtaDTO(RestaurantDTO restaurant, String eta) {
        this.restaurant = restaurant;
        this.eta = eta;
    }

    public RestaurantDTO getRestaurant() {
        return restaurant;
    }

    public String getEta() {
        return eta;
    }
}
