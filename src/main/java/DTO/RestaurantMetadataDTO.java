package DTO;

import Model.Location;
import Model.Restaurant;

import java.net.URL;

public class RestaurantMetadataDTO {
    private String id;
    private String name;
    private Location location;
    private URL logo;

    public RestaurantMetadataDTO(Restaurant restaurant) {
        this.id = restaurant.getId();
        this.name = restaurant.getName();
        this.location = restaurant.getLocation();
        this.logo = restaurant.getLogo();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public URL getLogo() {
        return logo;
    }
}
