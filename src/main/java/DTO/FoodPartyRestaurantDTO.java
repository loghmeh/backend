package DTO;

import Model.Food;
import Model.Location;
import Model.PartyFood;
import Model.Restaurant;

import java.net.URL;
import java.util.ArrayList;

public class FoodPartyRestaurantDTO {
    private String name;
    private String id;
    private URL logo;
    private Location location;
    private ArrayList<PartyFood> menu;

    public FoodPartyRestaurantDTO(Restaurant restaurant, ArrayList<PartyFood> activePartyFoods) {
        this.name = restaurant.getName();
        this.id = restaurant.getId();
        this.logo = restaurant.getLogo();
        this.location = restaurant.getLocation();
        this.menu = new ArrayList<>(activePartyFoods);
    }

    public FoodPartyRestaurantDTO() {
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public URL getLogo() {
        return logo;
    }

    public Location getLocation() {
        return location;
    }

    public ArrayList<PartyFood> getMenu() {
        return menu;
    }
}
