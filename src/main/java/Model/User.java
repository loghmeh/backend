package Model;

import java.util.ArrayList;
import java.util.Objects;

public class User {
    private Cart cart;
    private Integer id;
    private Location location;
    private String firstName;
    private String lastName;

    private String email;
    private String phoneNumber;
    private int balance = 0;
    private ArrayList<Order> orders;
//    private static User userInstance;

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public Cart getCart() {
        return cart;
    }

    public Integer getId() {
        return id;
    }

    public Location getLocation() {
        return location;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getBalance() {
        return balance;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
    }

    public User() {
    }

//    public User(Location location, String firsname, String lastname, String email, String phoneNumber) {
//
//
//    }

//    public User(Integer id, Location location, String firstName, String lastName, String email, String phoneNumber, int balance) {
//        this.id = id;
//        this.location = location;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.email = email;
//        this.phoneNumber = phoneNumber;
//        this.balance = balance;
////        this.cart = new Cart();
////        this.orders = new ArrayList<>();
//    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

//    private User() {
//        cart = new Cart();
//        location = new Location(0, 0);
//        firstName = "احسان";
//        lastName = "خامس‌پناه";
//        email = "info@khames.ie";
//        phoneNumber = "09396962772";
//        balance = 0;
//    }

//    public static User getInstance() {
//        if (userInstance == null)
//            userInstance = new User();
//        return userInstance;
//    }

    public ArrayList<Order> getOrders(){
        return orders;
    }

    public Order finalizeOrder() {
        Order newOrder = cart.finalizeOrder();
        orders.add(newOrder);
        return newOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return balance == user.balance &&
                Objects.equals(cart, user.cart) &&
                id.equals(user.id) &&
                Objects.equals(location, user.location) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                email.equals(user.email) &&
                phoneNumber.equals(user.phoneNumber) &&
                Objects.equals(orders, user.orders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cart, id, location, firstName, lastName, email, phoneNumber, balance, orders);
    }

    @Override
    public String toString() {
        return "User{" +
                "cart=" + cart +
                ", id=" + id +
                ", location=" + location +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", balance=" + balance +
                ", orders=" + orders +
                '}';
    }
}
