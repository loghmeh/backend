package Model;

public enum CartMode{
    EMPTY(0), NORMAL(1), FOODPARTY(2);

    private final int value;
    CartMode(int value) {
        this.value = value;
    }

    public static CartMode of(int value) {
        for (CartMode e : values()) {
            if (value == e.value) {
                return e;
            }
        }
        return null;
    }

    public int getValue() {
        return value;
    }
}
