package Model;

import Util.Time;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Order {
    private ArrayList<CartFoodItem> foods;
    private String restaurantId;
    private int id;
    private OrderStatus status;
    private LocalDateTime eta;

    public String getEta() {
        return Time.dateToString(eta);
    }

    public void setStatus (OrderStatus status) {
        this.status = status;
    }

    public Order(int id, ArrayList<CartFoodItem> foods, String restaurantId) {
        this.id = id;
        this.foods = foods;
        this.restaurantId = restaurantId;
        this.status = OrderStatus.TODELIVER;
    }

    public Order(int id, ArrayList<CartFoodItem> foods, String restaurantId, LocalDateTime eta, OrderStatus status) {
        this.id = id;
        this.foods = foods;
        this.restaurantId = restaurantId;
        this.status = status;
        this.eta = eta;
    }

    public Order(ArrayList<CartFoodItem> foods, String restaurantId) {
        this.foods = foods;
        this.restaurantId = restaurantId;
        this.status = OrderStatus.TODELIVER;
    }

    public Order( String restaurantId) {
        this.restaurantId = restaurantId;
        this.status = OrderStatus.TODELIVER;
    }

    public void setEta(LocalDateTime eta){
        this.eta = eta;
    }

    public ArrayList<CartFoodItem> getFoods() {
        return foods;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public int getId() {
        return id;
    }

    public int getTotalPrice() {
        return Util.FoodUtil.findTotalPrice(foods);
    }

    public OrderStatus getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Order{" +
                "foods=" + foods +
                ", restaurantId='" + restaurantId + '\'' +
                ", id=" + id +
                ", status=" + status +
                ", eta=" + eta +
                '}';
    }
}
