package Model;

import java.util.Objects;

public class CartFoodItem extends Food {
//    private Food food;
    private int count;

//    public Food getFood() {
//        return new Food(this);
//    }

    public int getCount() {
        return count;
    }

    public CartFoodItem(Food food, int count) {
        super(food);
//        this.food = food;
        this.count = count;
    }

    public void setCount(int count){
        this.count = count;
    }
    public void increaseCountBy(int increaseCount) {
        count += increaseCount;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        CartFoodItem that = (CartFoodItem) o;
//        return count == that.count &&
//                Objects.equals(food, that.food);
//    }

}
