package Model;

import java.time.LocalDateTime;
import java.util.HashMap;

public class Database {
    private static Database dbInstance;

    private HashMap<String, Restaurant> restaurants;
    private LocalDateTime currentFoodPartyFinishTime;

    private Database() {
        restaurants = new HashMap<>();
    }

    public static Database getInstance() {
        if (dbInstance == null)
            dbInstance = new Database();
        return dbInstance;
    }

    public LocalDateTime getCurrentFoodPartyFinishTime() {
        return currentFoodPartyFinishTime;
    }

    public void setCurrentFoodPartyFinishTime(LocalDateTime currentFoodPartyFinishTime) {
        this.currentFoodPartyFinishTime = currentFoodPartyFinishTime;
    }

    public HashMap<String, Restaurant> getRestaurants() {
        return restaurants;
    }

    public void addRestaurant(Restaurant newRestaurant) {
        restaurants.put(newRestaurant.getId(), newRestaurant);
    }
}
