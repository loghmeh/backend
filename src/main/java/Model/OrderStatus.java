package Model;

public enum OrderStatus {
    TODELIVER(0), DELIVERING(1), DELIVERED(2), CANCELED(3);

    @Override
    public String toString() {
        switch(this) {
            case TODELIVER: return "رستوران در انتظار پیک";
            case DELIVERING: return "پیک در راه";
            case DELIVERED: return "تحویل داده شد. نوش جان!";
            case CANCELED: return "لغو شده";
            default: throw new IllegalArgumentException();
        }
    }

    private final int value;
    OrderStatus(int value) {
        this.value = value;
    }

    public static OrderStatus of(int value) {
        for (OrderStatus e : values()) {
            if (value == e.value) {
                return e;
            }
        }
        return null;
    }

    public int getValue() {
        return value;
    }
}
