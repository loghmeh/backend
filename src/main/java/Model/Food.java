package Model;

import java.util.Objects;

public class Food {
    private String id;
    private String name;
    private String image;
    private String description;
    private float popularity;
    private int price;
    private String restaurantId;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Food() {
    }

    public Food(Food other) {
        id = other.id;
        name = other.name;
        image = other.image;
        description = other.description;
        popularity = other.popularity;
        price = other.price;
        restaurantId = other.restaurantId;
    }

    public Food(String id,
                String name,
                String image,
                String description,
                float popularity,
                int price,
                String restaurantId)
    {
        this.id = id;
        this.name = name;
        this.image = image;
        this.description = description;
        this.popularity = popularity;
        this.price = price;
        this.restaurantId = restaurantId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public float getPopularity() {
        return popularity;
    }

    public int getPrice() {
        return price;
    }

    public String getRestaurantId() { return restaurantId; }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }


    @Override
    public String toString() {
        return "Food{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", description='" + description + '\'' +
                ", popularity=" + popularity +
                ", price=" + price +
                ", restaurantId='" + restaurantId + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Food food = (Food) o;
        return Float.compare(food.popularity, popularity) == 0 &&
                price == food.price &&
                id.equals(food.id) &&
                name.equals(food.name) &&
                image.equals(food.image) &&
                description.equals(food.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, image, description, popularity, price, restaurantId);
    }
}
