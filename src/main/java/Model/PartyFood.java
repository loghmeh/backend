package Model;

import java.util.Objects;

public class PartyFood extends  Food{
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(int oldPrice) {
        this.oldPrice = oldPrice;
    }

    public PartyStatus getStatus() {
        return status;
    }

    public void setStatus(PartyStatus status) {
        this.status = status;
    }

    private int count;
    private int oldPrice;
    private PartyStatus status;

    public PartyFood() {
        status = PartyStatus.ACTIVE;
    }
    public PartyFood(Food food, int count, int oldPrice) {
        super(food);
        this.count = count;
        this.oldPrice = oldPrice;
        this.status = PartyStatus.ACTIVE;

    }

    public PartyFood(Food food, int count, int oldPrice, PartyStatus status) {
        super(food);
        this.count = count;
        this.oldPrice = oldPrice;
        this.status = status;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PartyFood partyFood = (PartyFood) o;
        return count == partyFood.count &&
                oldPrice == partyFood.oldPrice &&
                status == partyFood.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(count, oldPrice, status);
    }
}
