//package Model;
//
//import DTO.AddFoodPartyRestaurantRequest;
//import Commands.Request.AddRestaurantRequest;
//
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.HashMap;
//
//public class StoredRestaurant {
//    private String id;
//    private String name;
//    private Location location;
//    private URL logo;
//        private HashMap<String, Food> menu;
////    private ArrayList<Food> menu;
////    private ArrayList<PartyFood> partyMenu;;
//        private HashMap<String, PartyFood> partyMenu;
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public URL getLogo() {
//        return logo;
//    }
//
//    public void setLogo(URL logo) {
//        this.logo = logo;
//    }
//
//
//
//    public String getName() {
//        return name;
//    }
//
//
//    public Location getLocation() {
//        return location;
//    }
//
////    public ArrayList<Food> getMenu() {
////        return menu;
////    }
//    public HashMap<String, Food> getMenu() {
//        return menu;
//    }
//
//    public void addFood(Food newFood) {
//        menu.put(newFood.getName(), newFood);
////        menu.add(newFood);
//    }
//
//    public void addPartyFood(PartyFood newPartyFood) {
////        partyMenu.put(newPartyFood.getName(), newPartyFood);
//        partyMenu.add(newPartyFood);
//    }
//
////    public Restaurant(AddRestaurantRequest restaurantRequest) {
////        this.id = restaurantRequest.getId();
////        this.name = restaurantRequest.getName();
////        this.logo = restaurantRequest.getLogo();
////        this.location = restaurantRequest.getLocation();
////        this.partyMenu = new HashMap<>();
////        this.menu = new HashMap<>();
////        for (Food food : restaurantRequest.getMenu()) {
////            this.menu.put(food.getName(), food);
////        }
////    }
//
////    public Restaurant(AddFoodPartyRestaurantRequest restaurantRequest) {
////        this.id = restaurantRequest.getId();
////        this.name = restaurantRequest.getName();
////        this.logo = restaurantRequest.getLogo();
////        this.location = restaurantRequest.getLocation();
////        this.menu = new HashMap<>();
////        this.partyMenu = new HashMap<>();
////        for (PartyFood partyFood: restaurantRequest.getMenu()) {
////            this.partyMenu.put(partyFood.getName(), partyFood);
////        }
////    }
//
//    public ArrayList<PartyFood> getPartyMenu() {
//        return partyMenu;
//    }
////    public HashMap<String, PartyFood> getPartyMenu() {
////        return partyMenu;
////    }
//
//    @Override
//    public String toString() {
//        return "Restaurant{" +
//                "id='" + id + '\'' +
//                ", name='" + name + '\'' +
//                ", location=" + location +
//                ", logo=" + logo +
//                ", menu=" + menu +
//                '}';
//    }
//}
