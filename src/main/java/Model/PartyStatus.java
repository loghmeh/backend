package Model;

public enum PartyStatus {
    ACTIVE(0), INACTIVE(1);

    private final int value;
    PartyStatus(int value) {
        this.value = value;
    }

    public static PartyStatus of(int value) {
        for (PartyStatus e : values()) {
            if (value == e.value) {
                return e;
            }
        }
        return null;
    }

    public int getValue() {
        return value;
    }

}
