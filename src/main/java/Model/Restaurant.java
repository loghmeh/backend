package Model;

//import DTO.AddFoodPartyRestaurantRequest;
import Commands.Request.AddRestaurantRequest;
import DTO.FoodPartyRestaurantDTO;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

public class Restaurant {
    private String id;
    private String name;
    private Location location;
    private URL logo;
    private int foodIdCounter = java.lang.Math.abs((int)new Date().getTime());
//    private HashMap<String, Food> menu;
//    private HashMap<String, PartyFood> partyMenu;
    private ArrayList<Food> menu = new ArrayList<>();
    private ArrayList<PartyFood> partyMenu = new ArrayList<>();
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public URL getLogo() {
        return logo;
    }

    public void setLogo(URL logo) {
        this.logo = logo;
    }


    public String getName() {
        return name;
    }


    public Location getLocation() {
        return location;
    }

//    public HashMap<String, Food> getMenu() {
//        return menu;
//    }

    public void addFood(Food newFood) {
        String foodId = getNewNormalFoodId();
        newFood.setId(foodId);
        menu.add(newFood);
//        menu.put(foodId, newFood);
    }

    public void addPartyFood(PartyFood newPartyFood) {
        String foodId = getNewPartyFoodId();
        newPartyFood.setId(foodId);
        partyMenu.add(newPartyFood);
//        partyMenu.put(foodId, newPartyFood);
    }

    public Restaurant(String id, String name, Location location, URL logo) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.logo = logo;
        menu = new ArrayList<>();
        partyMenu = new ArrayList<>();
    }

    public Restaurant(AddRestaurantRequest restaurantRequest) {
        this.id = restaurantRequest.getId();
        this.name = restaurantRequest.getName();
        this.logo = restaurantRequest.getLogo();
        this.location = restaurantRequest.getLocation();
        this.partyMenu = new ArrayList<>();
        this.menu = new ArrayList<>();
        for (Food food : restaurantRequest.getMenu()) {
            String foodId = getNewNormalFoodId();
            food.setId(foodId);
            food.setRestaurantId(id);
            this.menu.add(food);
//            this.menu.put(foodId, food);
        }
    }

    public Restaurant(FoodPartyRestaurantDTO restaurantRequest) {
        this.id = restaurantRequest.getId();
        this.name = restaurantRequest.getName();
        this.logo = restaurantRequest.getLogo();
        this.location = restaurantRequest.getLocation();
        this.menu = new ArrayList<>();
        this.partyMenu = new ArrayList<>();
        for (PartyFood partyFood: restaurantRequest.getMenu()) {
            String foodId = getNewPartyFoodId();
            partyFood.setId(foodId);
            partyFood.setRestaurantId(id);
            this.partyMenu.add(partyFood);
//            this.partyMenu.put(foodId, partyFood);
        }
    }

    public void setFoodId(Food food) {
        if (food instanceof PartyFood) {
            food.setId(getNewPartyFoodId());
        }
        else {
            food.setId(getNewNormalFoodId());
        }
    }

    public ArrayList<Food> getMenu() {
        return menu;
    }

    public ArrayList<PartyFood> getPartyMenu() {
        return partyMenu;
    }

    public void setMenu(ArrayList<Food> menu) {
        this.menu = menu;
    }

    public void setPartyMenu(ArrayList<PartyFood> partyMenu) {
        this.partyMenu = partyMenu;
    }

    //    public HashMap<String, PartyFood> getPartyMenu() {
//        return partyMenu;
//    }

    private String getNewNormalFoodId() {
        return id + "_" + foodIdCounter++;
    }
    private String getNewPartyFoodId() {
        return id + "_" + foodIdCounter++ + "_p";
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", location=" + location +
                ", logo=" + logo +
                ", menu=" + menu +
                ", partyMenu=" + partyMenu +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Restaurant that = (Restaurant) o;
        return id.equals(that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(location.getX(), that.location.getX()) &&
                Objects.equals(location.getY(), that.location.getY()) &&
                Objects.equals(logo.toString(), that.logo.toString()) &&
                Objects.equals(menu, that.menu) &&
                Objects.equals(partyMenu, that.partyMenu);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, location.getX(), location.getY(), logo.toString(), menu, partyMenu);
    }
}
