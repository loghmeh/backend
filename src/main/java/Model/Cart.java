package Model;

import Data.Access.DatabaseAccess;
import Data.Access.UserSessionAccess;
import Exceptions.*;
import Util.FoodUtil;

import java.util.ArrayList;
import java.util.Objects;

public class Cart {
    private ArrayList<CartFoodItem> foods = new ArrayList<>();
    private String restaurantId; // Convert to Restaurant Metadata?
    private CartMode cartMode;

    public void setFoods(ArrayList<CartFoodItem> foods) {
        this.foods = foods;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public void setCartMode(CartMode cartMode) {
        this.cartMode = cartMode;
    }

    public CartMode getCartMode() {
        return cartMode;
    }

    public Order finalizeOrder() {
        int cost = getTotalPrice();
        try {
            UserSessionAccess.depositAccount(cost);
        } catch(NegativeUserBalanceException e){
            //TODO: Remove exception from Access Layer
        }
        ArrayList<CartFoodItem> orderFoods = new ArrayList<>(foods);
        Order order = new Order(orderFoods, this.restaurantId);
        restaurantId = null;
        cartMode = null;
        foods.clear();
        return order;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public int getTotalPrice() {
        return Util.FoodUtil.findTotalPrice(foods);
    }

    public void addFood(String restaurantId, String foodId) throws FoodDoesntExistException,
            RestaurantDoesntExistException,
            CartCanOnlyContainFoodsFromOneRestaurantException, CartCanOnlyContainFoodsFromOneCategoryException {
        if (this.restaurantId != null && !this.restaurantId.equals(restaurantId)) {
            throw new CartCanOnlyContainFoodsFromOneRestaurantException();
        }
        if(cartMode != null && cartMode.equals(CartMode.FOODPARTY)) {
            throw new CartCanOnlyContainFoodsFromOneCategoryException();
        }
        Food newFood = DatabaseAccess.getFood(restaurantId, foodId);
        if (this.restaurantId == null) {
            this.restaurantId = restaurantId;
        }
        if (cartMode == null) {
            cartMode = CartMode.NORMAL;
        }
        for (CartFoodItem foodItem : foods) {
            if (newFood.equals(foodItem)) {
                foodItem.increaseCountBy(1);
                return;
            }
        }
        foods.add(new CartFoodItem(newFood, 1));
    }

    public ArrayList<CartFoodItem> getFoods() {
        return foods;
    }

    public void addPartyFood(String restaurantId, String foodId)
            throws CartCanOnlyContainFoodsFromOneCategoryException,
            CartCanOnlyContainFoodsFromOneRestaurantException,
            FoodDoesntExistException, RestaurantDoesntExistException {
        if (this.restaurantId != null && !this.restaurantId.equals(restaurantId)) {
            throw new CartCanOnlyContainFoodsFromOneRestaurantException();
        }
        if(cartMode != null && cartMode.equals(CartMode.NORMAL)) {
            throw new CartCanOnlyContainFoodsFromOneCategoryException();
        }

        if (this.restaurantId == null) {
            this.restaurantId = restaurantId;
        }
        if (cartMode == null) {
            cartMode = CartMode.FOODPARTY;
        }
        Food newFood = DatabaseAccess.getPartyFood(restaurantId, foodId);
        for (CartFoodItem foodItem : foods) {
            if (newFood.equals(foodItem)) {
                foodItem.increaseCountBy(1);
                return;
            }
        }
        foods.add(new CartFoodItem(newFood, 1));
    }

    public void deleteFood(String restaurantId, String foodId)
            throws FoodDoesntExistException, RestaurantDoesntExistException, FoodDoesntExistInCartException{
        Food deletedFood = FoodUtil.isPartyFood(foodId)
                ? DatabaseAccess.getPartyFood(restaurantId, foodId)
                : DatabaseAccess.getFood(restaurantId, foodId);

        boolean foodExistsInCart = false;
        for (CartFoodItem foodItem : foods) {
            if (deletedFood.equals(foodItem)) {
                foodItem.increaseCountBy(-1);
                foodExistsInCart = true;
                break;
            }
        }
        if (!foodExistsInCart) {
            throw new FoodDoesntExistInCartException();
        }
        foods.removeIf(food -> food.getCount() == 0);
        if (foods.size() == 0) {
            this.restaurantId = null;
            cartMode = null;
        }
    }

    @Override
    public String toString() {
        return "Cart{" +
                "foods=" + foods +
                ", restaurantId='" + restaurantId + '\'' +
                ", cartMode=" + cartMode +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cart cart = (Cart) o;
        return Objects.equals(foods, cart.foods) &&
                Objects.equals(restaurantId, cart.restaurantId) &&
                cartMode == cart.cartMode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(foods, restaurantId, cartMode);
    }
}
