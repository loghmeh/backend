package Repository.jdbc.Order;

import Model.Order;
import Repository.jdbc.IRepository;

import java.util.ArrayList;

public interface IOrderRepository extends IRepository<Order, Integer> {
    ArrayList<Order> findByUserId(int userId);
    Integer add(Order order, int userId);
    void updateStatus(int orderId, int status);
    void updateEta(Order order);
    void updateStatusAndEta(int orderId, int status, String eta);
}
