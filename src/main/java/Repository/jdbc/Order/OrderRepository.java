package Repository.jdbc.Order;

import Model.CartFoodItem;
import Model.Order;
import Model.OrderStatus;
import Repository.jdbc.Database;
import Repository.jdbc.Repository;

import java.sql.*;
import java.util.ArrayList;

import static Util.Time.stringToDate;

public class OrderRepository extends Repository<Order, Integer> implements IOrderRepository {

    public String getFindByUserIdStatement(){
        return "SELECT id, rest_id, status, eta"+
                " FROM orders"+
                " WHERE user_id = ?";

    }

    @Override
    protected String getFindStatement() {
        return "SELECT"+
                " cart_items.order_id,"+
                " cart_items.food_id,"+
                " orders.rest_id,"+
                " orders.status,"+
                " orders.id,"+
                " orders.eta"+
                " FROM orders"+
                " LEFT JOIN cart_items"+
                " ON orders.id = cart_items.order_id"+
                " WHERE orders.id = ?";
    }

    public String getAddStatement(){
        return "INSERT INTO orders ("
                +" id,"
                +" rest_id,"
                +" status,"
                +" eta,"
                +" user_id ) VALUES ("
                +"NULL, ?, ?, ?, ?)";
    }

    public String getUpdateStatusStatement(){
        return "UPDATE orders" +
                " SET status = ?"+
                " WHERE id = ?";
    }

    public String getUpdateEtaStatement(){
        return "UPDATE orders" +
                " SET eta = ?"+
                " WHERE id = ?";
    }

    public String getUpdateStatusAndEtaStatement(){
        return "UPDATE orders" +
                " SET status = ?, eta = ?"+
                " WHERE id = ?";
    }

    @Override
    protected Order convertResultToModel(ResultSet rs) throws SQLException {
        ArrayList<CartFoodItem> foods = Database.cartFoodItemRepository.findByOrderId(rs.getInt("id"));
        return new Order(
                rs.getInt("id"),
                foods,
                rs.getString("rest_id"),
                stringToDate(rs.getString("eta")),
                OrderStatus.of(rs.getInt("status"))
        );
    }

    @Override
    public ArrayList<Order> findByUserId(int userId) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getFindByUserIdStatement())) {
            st.setInt(1, userId);
            ResultSet rs = null;
            try{
                rs = st.executeQuery();
                ArrayList<Order> orders = new ArrayList<>();
                while(rs.next()){
                    Order order = convertResultToModel(rs);
                    orders.add(order);
                }
                return orders;
            }catch (SQLException ex){
                System.out.println("error in OrderRepository.findByUserId");
            }finally {
                if(rs != null)
                    rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer add(Order order, int userId) {
        try(Connection con = Database.getConnection();
        PreparedStatement st = con.prepareStatement(getAddStatement(), Statement.RETURN_GENERATED_KEYS)) {
            st.setString(1, order.getRestaurantId());
            st.setInt(2, order.getStatus().getValue());
            st.setString(3, order.getEta().toString());
            st.setInt(4,userId);
            try {
                st.executeUpdate();
                ResultSet rs = st.getGeneratedKeys();
                if(rs.next())
                    return rs.getInt(1);

            }catch (SQLException ex) {
                System.out.println("error in OrderRepository.add");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void updateStatus(int orderId, int status) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getUpdateStatusStatement())) {
            st.setInt(1, status);
            st.setInt(2, orderId);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println("error in OrderRepository.updateStatus");
            e.printStackTrace();
        }
    }

    @Override
    public void updateEta(Order order) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getUpdateEtaStatement())) {
            st.setString(1,order.getEta());
            st.setInt(2, order.getId());
            try {
                st.executeUpdate();
            }catch (SQLException ex) {
                System.out.println("error in OrderRepository.updateEta");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void updateStatusAndEta(int orderId, int status, String eta) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getUpdateStatusAndEtaStatement())) {
            st.setInt   (1, status);
            st.setString(2, eta);
            st.setInt   (3, orderId);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println("error in OrderRepository.updateEta");
            e.printStackTrace();
        }
    }
}
