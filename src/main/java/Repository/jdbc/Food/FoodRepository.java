package Repository.jdbc.Food;

import Model.Food;
import Repository.jdbc.Database;
import Repository.jdbc.Repository;

import javax.xml.crypto.Data;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FoodRepository extends Repository<Food, String> implements IFoodRepository {

    @Override
    public String getFindStatement() {
        return "SELECT *"+
                " FROM foods"+
                " WHERE id = ?";

    }

    public String getFindByRestaurantIdStatement() {
        return "SELECT *"+
                " FROM foods"+
                " WHERE rest_id = ?";

    }
    public String getAddStatement() {
        return "INSERT INTO foods ("
                + " id,"
                + " name,"
                + " image,"
                + " description,"
                + " popularity,"
                + " price,"
                + " rest_id ) VALUES ("
                + "?, ?, ?, ?, ?, ?, ?)";
    }

    public String getDeleteStatement(){
        return "DELETE"+
                " FROM foods"+
                " WHERE id = ?";

    }

    public String getUpdatePriceStatement(){
        return "UPDATE foods"+
                " SET price = ?"+
                " WHERE id = ?";
    }

    public String getSearchRestaurantIdsByFoodNameStatement(){
        return "SELECT DISTINCT r.id " +
                "FROM foods JOIN restaurants r on foods.rest_id = r.id " +
                "WHERE foods.name LIKE ?";
    }

    @Override
    protected Food convertResultToModel(ResultSet rs) throws SQLException {
        return new Food(
                rs.getString("id"),
                rs.getString("name"),
                rs.getString("image"),
                rs.getString("description"),
                rs.getFloat("popularity"),
                rs.getInt("price"),
                rs.getString("rest_id")
        );
    }

    @Override
    public ArrayList<Food> findByRestaurantId(String restId) {
        try(Connection con = Database.getConnection();
        PreparedStatement st = con.prepareStatement(getFindByRestaurantIdStatement())) {
            st.setString(1, restId);
            ResultSet rs;
            try{
                rs = st.executeQuery();
                ArrayList<Food> foods = new ArrayList<>();
                while(rs.next()){
                    Food food = convertResultToModel(rs);
                    foods.add(food);
                }
                return foods;
            } catch (SQLException e) {
               System.out.println("error in FoodRepository.findByRestaurantId query.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean existsById(String foodId) {
        try(Connection con = Database.getConnection();
        PreparedStatement st = con.prepareStatement(getFindStatement())) {
            st.setString(1, foodId);
            ResultSet rs = null;
            try{
                rs = st.executeQuery();
                if(rs.next()){
                    return true;
                }
                return false;
            } catch (SQLException e) {
                System.out.println("error in FoodRepository.existById query.");
            } finally {
                if(rs != null)
                    rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void add(Food food) {
        try(Connection con = Database.getConnection();
        PreparedStatement st = con.prepareStatement(getAddStatement())) {
            st.setString(1, food.getId());
            st.setString(2, food.getName());
            st.setString(3, food.getImage());
            st.setString(4, food.getDescription());
            st.setFloat(5, food.getPopularity());
            st.setInt(6, food.getPrice());
            st.setString(7, food.getRestaurantId());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(" error in FoodRepository.add.");
            e.printStackTrace();
        }
    }

    @Override
    public void addBulk(ArrayList<Food> foods) {
        for(Food food: foods){
            add(food);
        }
    }

    @Override
    public void delete(String foodId) {
        try(Connection con = Database.getConnection();
        PreparedStatement st = con.prepareStatement(getDeleteStatement())) {
            st.setString(1, foodId);
            int row;
            row = st.executeUpdate();
            System.out.println(row + " rows successfully deleted from FoodsRepository");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updatePrice(Food food) {
        try(Connection con = Database.getConnection();
        PreparedStatement st = con.prepareStatement(getUpdatePriceStatement())) {
            st.setInt(1, food.getPrice());
            st.setString(2, food.getId());
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<String> searchRestaurantIdsByFoodName(String searchName) {
        try(Connection con = Database.getConnection();
        PreparedStatement st = con.prepareStatement(getSearchRestaurantIdsByFoodNameStatement())) {
            st.setString(1, "%" + searchName + "%");
            ResultSet rs = rs = st.executeQuery();
            ArrayList<String> restaurantIds = new ArrayList<>();
            while(rs.next()) {
                restaurantIds.add(rs.getString(1));
            }
            return restaurantIds;
        } catch (SQLException e) {
            System.out.println("error in FoodRepository.searchByName");
            e.printStackTrace();
        }
        return null;
    }


}
