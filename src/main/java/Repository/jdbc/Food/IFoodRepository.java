package Repository.jdbc.Food;

import Model.Food;
import Repository.jdbc.IRepository;

import java.util.ArrayList;

public interface IFoodRepository extends IRepository<Food, String> {
    ArrayList<Food> findByRestaurantId(String restId);
    boolean existsById(String foodId);
    void add(Food food);
    void addBulk(ArrayList<Food> foods);
    void delete(String foodId);
    void updatePrice(Food food);
    ArrayList<String> searchRestaurantIdsByFoodName(String searchName);
}
