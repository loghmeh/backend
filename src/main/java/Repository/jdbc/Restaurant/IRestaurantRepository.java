package Repository.jdbc.Restaurant;

import Model.Restaurant;
import Repository.jdbc.IRepository;

import java.util.ArrayList;

public interface IRestaurantRepository extends IRepository<Restaurant, String> {
    void addRestaurant(Restaurant restaurant);
    ArrayList<Restaurant> searchByName(String searchName);
    ArrayList<Restaurant> find(int pageId, int pageChunk);
}
