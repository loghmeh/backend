package Repository.jdbc.Restaurant;

import Model.Food;
import Model.Location;
import Model.PartyFood;
import Model.Restaurant;
import Repository.jdbc.Database;
import Repository.jdbc.Repository;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;

public class RestaurantRepository extends Repository<Restaurant, String> implements IRestaurantRepository{
    private final String tableName = "restaurants";
    private final LinkedHashMap<String, Class> cols = new LinkedHashMap<String, Class>() {{
        put("id", String.class);
        put("name", String.class);
        put("loc_x", String.class);
        put("loc_y", Double.class);
        put("logo", String.class);
    }};

    private String getColumnsSqlString() {
        return String.join(",", cols.keySet());
    }

    private String getAddStatement() {
        return "INSERT INTO " + tableName
                + " (" + getColumnsSqlString() + ") "
                + "VALUES " +
                "(?, ?, ?, ?, ?)";
    }

    private String getExistsStatement() {
        return "SELECT (COUNT(*) > 0) as found" +
                " FROM " + tableName +
                " WHERE id = ?";
    }

    protected String getFindAllStatement() {
        return "SELECT *" +
                " FROM " + tableName;
    }

    @Override
    protected String getFindStatement() {
        return "SELECT *" +
                " FROM " + tableName +
                " WHERE id = ?";
    }

    public String getSearchByNameStatement(){
        return "SELECT *" +
                " FROM " + tableName +
                " WHERE name LIKE ? ";
    }

    public String getFindChunkStatement(){
        return "SELECT *" +
                " FROM " + tableName +
                " LIMIT ?, ?";

    }

    public String getCountStatement(){
        return "SELECT COUNT(*) FROM " + tableName;
    }

    @Override
    public Restaurant convertResultToModel(ResultSet rs) throws SQLException {
        try {
            String resteaurantId = rs.getString(1);
            String restaurantName = rs.getString(2);
            double restaurantLocX = rs.getDouble(3);
            double restaurantLocY = rs.getDouble(4);
            URL restaurantLogo = new URL(rs.getString(5));
            Restaurant newRestaurant = new Restaurant(
                    resteaurantId,
                    restaurantName,
                    new Location(restaurantLocX, restaurantLocY),
                    restaurantLogo
            );
            ArrayList<Food> foods = Database.foodRepository.findByRestaurantId(resteaurantId);
            ArrayList<Food> filteredFoods = foods.stream()
                    .filter(food -> !Util.FoodUtil.isPartyFood(food.getId()))
                    .collect(Collectors.toCollection(ArrayList::new));
            ArrayList<PartyFood> partyFoods = Database.partyFoodRepository.findByRestaurantId(resteaurantId);
            newRestaurant.setMenu(filteredFoods);
            newRestaurant.setPartyMenu(partyFoods);
            return newRestaurant;
        } catch (MalformedURLException e) {
            // ignore
        }
        return null;
    }

    public void addRestaurant(Restaurant restaurant) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getAddStatement())) {
            st.setString(1, restaurant.getId());
            st.setString(2, restaurant.getName());
            st.setDouble(3, restaurant.getLocation().getX());
            st.setDouble(4, restaurant.getLocation().getY());
            st.setString(5, restaurant.getLogo().toString());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println("error in RestaurantRepository.addRestaurant query.");
            e.printStackTrace();
        }
//        try { if (rs != null) rs.close(); } catch (Exception e) {};
//        try { if (st != null) st.close(); } catch (Exception e) {};
//        try { if (con != null) con.close(); } catch (Exception e) {};
    }

    public boolean restaurantExists(String id) {
        try(Connection con = Database.getConnection();
            PreparedStatement stmt = con.prepareStatement(getExistsStatement())) {
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (SQLException e) {
            System.out.println("error in RestaurantRepository.restaurantExists query.");
            e.printStackTrace();
        }
        return false;
    }

    public ArrayList<Restaurant> findAll() {
        try(Connection con = Database.getConnection();
            PreparedStatement stmt = con.prepareStatement(getFindAllStatement())) {
            ResultSet rs = stmt.executeQuery();
            ArrayList<Restaurant> restaurants = new ArrayList<>();
            while (rs.next()) {
                Restaurant restaurant = convertResultToModel(rs);
                restaurants.add(restaurant);
            }
            return restaurants;
        } catch (SQLException e) {
            System.out.println("error in Mapper.findByID query.");

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Restaurant findById(String id) {
        try(Connection con = Database.getConnection();
            PreparedStatement restaurantStmt = con.prepareStatement(getFindStatement())) {
            restaurantStmt.setString(1, id);
            ResultSet rs = restaurantStmt.executeQuery();
            Restaurant newRestaurant = null;
            if (rs.next()) {
                newRestaurant = convertResultToModel(rs);
            }
            return newRestaurant;
        } catch (SQLException e) {
            System.out.println("error in Mapper.findByID query.");

            e.printStackTrace();
        }
        return null;

    }

    @Override
    public ArrayList<Restaurant> searchByName(String searchName) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getSearchByNameStatement())) {
            st.setString(1, "%" + searchName + "%");
            ResultSet rs = null;
            try{
                rs = st.executeQuery();
                ArrayList<Restaurant> restaurants = new ArrayList<>();
                while(rs.next()){
                    Restaurant restaurant = convertResultToModel(rs);
                    String id = restaurant.getId();
                    ArrayList<Food> foods = Database.foodRepository.findByRestaurantId(id);
                    ArrayList<Food> filteredFoods = foods.stream()
                            .filter(food -> !Util.FoodUtil.isPartyFood(food.getId()))
                            .collect(Collectors.toCollection(ArrayList::new));
                    ArrayList<PartyFood> partyFoods = Database.partyFoodRepository.findByRestaurantId(id);
                    restaurant.setMenu(filteredFoods);
                    restaurant.setPartyMenu(partyFoods);
                    restaurants.add(restaurant);
                }
                return restaurants;
            }catch(SQLException e){
                System.out.println("error in RestaurantRepository.searchByName");
                e.printStackTrace();
            }finally {
                if(rs != null)
                    rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ArrayList<Restaurant> find(int pageId, int pageChunk) {
        try(Connection con = Database.getConnection();
        PreparedStatement st = con.prepareStatement(getFindChunkStatement())) {
            int begin = (pageId - 1) * pageChunk;
            st.setInt(1, begin);
            st.setInt(2, pageChunk);
            ResultSet rs = st.executeQuery();
            ArrayList<Restaurant> restaurants = new ArrayList<>();
            while (rs.next()) {
                Restaurant restaurant = convertResultToModel(rs);
                restaurants.add(restaurant);
            }
            return restaurants;
        } catch (SQLException e) {
            System.out.println("error in RestaurantRepository.findChunk.");
            e.printStackTrace();
        }
        return null;
    }


}
