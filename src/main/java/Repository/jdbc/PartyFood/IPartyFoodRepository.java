package Repository.jdbc.PartyFood;

import Model.PartyFood;
import Repository.jdbc.IRepository;

import javax.servlet.http.Part;
import java.util.ArrayList;

public interface IPartyFoodRepository extends IRepository<PartyFood, String> {
    void add(PartyFood partyFood);
    void addBulk(ArrayList<PartyFood> partyFoods);
    void delete(PartyFood partyFood);
    void updateStatus(PartyFood partyFood);
    void updateCount(PartyFood partyFood);
    void updateAllStatus();
    ArrayList<PartyFood> findByRestaurantId(String restaurantId);
    boolean existsById(String partyFoodId);
    boolean sufficientCount(int count, String foodId);
}
