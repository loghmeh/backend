package Repository.jdbc.PartyFood;

import Model.Food;
import Model.PartyFood;
import Model.PartyStatus;
import Repository.jdbc.Database;
import Repository.jdbc.Repository;

import javax.servlet.http.Part;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PartyFoodRepository extends Repository<PartyFood, String> implements IPartyFoodRepository {
    @Override
    protected String getFindStatement() {
        return "SELECT"+
                " foods.id,"+
                " foods.name,"+
                " foods.image,"+
                " foods.description,"+
                " foods.popularity,"+
                " foods.price,"+
                " foods.rest_id,"+
                " party_foods.old_price,"+
                " party_foods.count,"+
                " party_foods.status"+
                " FROM party_foods"+
                " JOIN foods"+
                    " ON party_foods.food_id = foods.id"+
                " WHERE party_foods.food_id = ?";
    }


    protected String getAddStatement() {
        return "INSERT INTO party_foods (" +
                " count,"+
                " old_price,"+
                " status,"+
                " food_id ) VALUES ("+
                " ?, ?, ?, ? )";
    }

    public String getFindByRestaurantIdStatement() {
        return "SELECT"+
                " foods.id,"+
                " foods.name,"+
                " foods.image,"+
                " foods.description,"+
                " foods.popularity,"+
                " foods.price,"+
                " foods.rest_id,"+
                " party_foods.old_price,"+
                " party_foods.count,"+
                " party_foods.status"+
                " FROM party_foods"+
                " JOIN foods"+
                " ON party_foods.food_id = foods.id"+
                " WHERE foods.rest_id = ?";
    }

    public String getUpdateStatusStatement() {
        return "UPDATE party_foods"+
                " SET status = ?"+
                " WHERE food_id = ?";
    }

    public String getUpdateCountStatement() {
        return "UPDATE party_foods"+
                " SET count = ?"+
                " WHERE food_id = ?";
    }

    private String getPartyFoodExistsStatement() {
        return "SELECT COUNT(*) > 0" +
                " FROM party_foods" +
                " WHERE food_id = ?";
    }

    public String getUpdateAllStatement() {
        return "UPDATE party_foods"+
                " SET status = ?"+
                " WHERE status = ?";
    }

    public String getCheckCountStatement(){
        return "SELECT party_foods.count > ?" +
                " FROM party_foods" +
                " WHERE party_foods.food_id = ?";
    }

    @Override
    protected PartyFood convertResultToModel(ResultSet rs) throws SQLException {
        Food food = new Food(
                rs.getString("id"),
                rs.getString("name"),
                rs.getString("image"),
                rs.getString("description"),
                rs.getFloat("popularity"),
                rs.getInt("price"),
                rs.getString("rest_id")
        );
        PartyFood partyFood = new PartyFood(
                food,
                rs.getInt("count"),
                rs.getInt("old_price"),
                PartyStatus.of(rs.getInt("status"))
        );
        return partyFood;
    }

    @Override
    public void add(PartyFood partyFood) {
        Database.foodRepository.add(partyFood);
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getAddStatement())) {
            st.setInt(1, partyFood.getCount());
            st.setInt(2,partyFood.getOldPrice());
            st.setInt(3,partyFood.getStatus().getValue());
            st.setString(4, partyFood.getId());
            int row =  st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addBulk(ArrayList<PartyFood> partyFoods){
        for(PartyFood partyFood: partyFoods){
            add(partyFood);
        }
    }

    @Override
    public void delete(PartyFood partyFood) {
        Database.foodRepository.delete(partyFood.getId());
    }

    @Override
    public void updateStatus(PartyFood partyFood) {
        try(Connection con = Database.getConnection();
        PreparedStatement st = con.prepareStatement(getUpdateStatusStatement())) {
            st.setInt(1, partyFood.getStatus().getValue());
            st.setString(2, partyFood.getId());
            int row = st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateCount(PartyFood partyFood) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getUpdateCountStatement())) {
            st.setInt(1, partyFood.getCount());
            st.setString(2, partyFood.getId());
            int row = st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAllStatus() {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getUpdateAllStatement())) {
            st.setInt(1, PartyStatus.INACTIVE.getValue());
            st.setInt(2, PartyStatus.ACTIVE.getValue());
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<PartyFood> findByRestaurantId(String restaurantId) {
        try(Connection con = Database.getConnection();
        PreparedStatement st = con.prepareStatement(getFindByRestaurantIdStatement())) {
            ResultSet rs;
            st.setString(1, restaurantId);
            try{
                rs = st.executeQuery();
                ArrayList<PartyFood> partyFoods = new ArrayList<>();
                while(rs.next()){
                    PartyFood partyFood = convertResultToModel(rs);
                    partyFoods.add(partyFood);
                }
                return partyFoods;
            }catch(SQLException ex){
                System.out.println("error in PartyFoodRepository.findByRestaurantId");
                ex.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean existsById(String partyFoodId) {
        try(Connection con = Database.getConnection();
        PreparedStatement st = con.prepareStatement(getPartyFoodExistsStatement())) {
            st.setString(1, partyFoodId);
            ResultSet rs = null;
                rs = st.executeQuery();
                if(rs.next()){
                    return rs.getBoolean(1);
                }
                return false;
        } catch (SQLException e) {
            System.out.println("error in PartyFoodRepository.existsById");
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean sufficientCount(int count, String foodId) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getCheckCountStatement())) {
            st.setInt(1, count);
            st.setString(2, foodId);
            ResultSet rs = null;
            rs = st.executeQuery();
            if(rs.next()){
                return rs.getBoolean(1);
            }
            return false;
        } catch (SQLException e) {
            System.out.println("error in PartyFoodRepository.sufficientCount");
            e.printStackTrace();
        }
        return false;
    }
}
