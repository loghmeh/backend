package Repository.jdbc;

import Config.Loghmeh;
import Repository.jdbc.Cart.CartRepository;
import Repository.jdbc.CartFoodItem.CartFoodItemRepository;
import Repository.jdbc.Food.FoodRepository;
import Repository.jdbc.Order.OrderRepository;
import Repository.jdbc.PartyFood.PartyFoodRepository;
import Repository.jdbc.Restaurant.RestaurantRepository;
import Repository.jdbc.User.UserRepository;
import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class Database {

    private static BasicDataSource ds = new BasicDataSource();

    private final static String dbms = "mysql";
    private final static String dbHost = Loghmeh.DB_HOST;
    private final static String dbPort = Loghmeh.DB_PORT;

    private final static String username = Loghmeh.DB_USER;
    private final static String password = Loghmeh.DB_PASS;
    private final static String dbName = Loghmeh.DB_NAME;

    static {
        ds.setUrl("jdbc:" +
                Database.dbms +
                "://" + Database.dbHost +
                ":" + Database.dbPort +
                "/" + Database.dbName);
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setMinIdle(5);
        ds.setMaxIdle(10);
        ds.setMaxOpenPreparedStatements(100);
    }

    public static UserRepository userRepository;
    public static RestaurantRepository restaurantRepository;
    public static FoodRepository foodRepository;
    public static PartyFoodRepository partyFoodRepository;
    public static OrderRepository orderRepository;
    public static CartFoodItemRepository cartFoodItemRepository;
    public static CartRepository cartRepository;

    static{
        userRepository = new UserRepository();
        restaurantRepository = new RestaurantRepository();
        foodRepository = new FoodRepository();
        partyFoodRepository = new PartyFoodRepository();
        orderRepository = new OrderRepository();
        cartFoodItemRepository = new CartFoodItemRepository();
        cartRepository = new CartRepository();

    }

    public static Connection getConnection() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
        try {
            return ds.getConnection();
        } catch (SQLException ex) {
            System.out.println("----un: " + username + " | pw: " + password + " | host: " + dbHost + " | name: " + dbName);
            ex.printStackTrace();
            System.exit(1);
        }
        return null;
    }

}
