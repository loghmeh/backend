package Repository.jdbc.User;

import Model.User;
import Repository.jdbc.IRepository;

public interface IUserRepository extends IRepository<User, Integer> {
    Integer addUser(User user, String password);
    boolean userExists(User user);
    boolean userExists(String email);
    boolean checkPassword(int userId, String password);

}
