package Repository.jdbc.User;

import Data.Access.UserSessionAccess;
import Exceptions.UserNotFoundException;
import Model.Food;
import Model.Location;
import Model.User;
import Repository.jdbc.Cart.CartRepository;
import Repository.jdbc.Database;
import Repository.jdbc.IRepository;
import Repository.jdbc.Repository;
import Util.Hash.Hashing;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class UserRepository extends Repository<User, Integer> implements IUserRepository {
    private final String tableName = "users";
    private final LinkedHashMap<String, Class> cols = new LinkedHashMap<String, Class>() {{
        // put("id", Integer.class); // Because it is set in DB
        put("first_name", String.class);
        put("last_name", String.class);
        put("password", String.class);
        put("loc_x", Double.class);
        put("loc_y", Double.class);
        put("email", String.class);
        put("phone_number", String.class);
        put("balance", Integer.class);
    }};

    private String getColumnsSqlString() {
        return String.join(",", cols.keySet());
    }

    private String getSelectUserIdByEmailAndPasswordStatement() {
        return "SELECT" +
                " id" +
                " FROM " + tableName +
                " WHERE email = ? AND password = MD5(?)";
    }

    @Override
    protected String getFindStatement() {
        return "SELECT" +
                    " id,"+
                    " first_name,"+
                    " last_name," +
                    " loc_x,"+
                    " loc_y,"+
                    " email,"+
                    " phone_number,"+
                    " balance"+
                " FROM " + tableName +
                " WHERE id = ?";
    }

    private String getAddStatement() {
        return "INSERT INTO " + tableName
                + " (" + getColumnsSqlString() + ") "
                + "VALUES " +
                "(?, ?, MD5(?), ?, ?, ?, ?, ?)";
    }

    private String getIncreaseBalanceStatement() {
        return "UPDATE " + tableName
                + " SET balance = balance + ?"
                + " WHERE id = ?";
    }

    private String getUserIdByEmailStatement() {
        return "SELECT " +
                " id" +
                " FROM " + tableName +
                " WHERE email = ?";

    }

    protected String getCheckPasswordStatement() {
        return "SELECT" +
                " password"+
                " FROM " + tableName +
                " WHERE id = ?";
    }

    private String getExistsByEmailStatement() {return "SELECT (COUNT(*) > 0) as found FROM " + tableName + " WHERE email = ?";}
    private String getExistsStatement() {
        return "SELECT (COUNT(*) > 0) as found FROM " + tableName + " WHERE id = ?";
    }

    @Override
    protected User convertResultToModel(ResultSet rs) throws SQLException {
        Integer userId = rs.getInt(1);
        String userFirstName = rs.getString(2);
        String userLastName = rs.getString(3);
        double userLocX = rs.getDouble(4);
        double userLocY = rs.getDouble(5);
        String userEmail = rs.getString(6);
        String userPhoneNumber = rs.getString(7);
        int userBalance = rs.getInt(8);
        User user = new User();
        user.setId(userId);
        user.setLocation(new Location(userLocX, userLocY));
        user.setFirstName(userFirstName);
        user.setLastName(userLastName);
        user.setEmail(userEmail);
        user.setPhoneNumber(userPhoneNumber);
        user.setBalance(userBalance);
//        User user = new User(
//                userId,
//                new Location(userLocX, userLocY),
//                userFirstName,
//                userLastName,
//                userEmail,
//                userPhoneNumber,
//                userBalance
//        );
        return user;
    }

    public Integer getUserIdByEmailAndPassword(String email, String password) {
//        String hashedPassword = Hashing.hash(password);
//        System.out.println(hashedPassword);
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getSelectUserIdByEmailAndPasswordStatement())) {
            st.setString(1, email);
            st.setString(2, password);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("error in UserRepository.userExists query.");
        }
        return null;
    }

    public boolean userExists(User user) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getExistsStatement())) {
            st.setInt(1, user.getId());

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (SQLException e) {
            System.out.println("error in UserRepository.userExists query.");
        }
        return false;
    }

    @Override
    public boolean userExists(String email) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getExistsByEmailStatement())) {
            st.setString(1, email);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (SQLException e) {
            System.out.println("error in UserRepository.userExists query.");
        }
        return false;
    }

    @Override
    public boolean checkPassword(int userId, String password) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getCheckPasswordStatement())) {
            st.setInt(1, userId);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                String hash = rs.getString(1);
                return Hashing.verify(password, hash);
            }
            else{
                throw new UserNotFoundException();
            }
        } catch (SQLException e) {
            System.out.println("error in UserRepository.checkPassword.");
        } catch(UserNotFoundException e){
            System.out.println("user not found.");
        }
        return false;
    }

//    public User findUserById(int id) {
//        try(Connection con = Database.getConnection();
//            PreparedStatement st = con.prepareStatement(getFindStatement())) {
//            st.setInt(1, id);
//
//            ResultSet rs = st.executeQuery();
//            if (rs.next()) {
//                Integer userId = rs.getInt(1);
//                String userFirstName = rs.getString(2);
//                String userLastName = rs.getString(3);
//                double userLocX = rs.getDouble(4);
//                double userLocY = rs.getDouble(5);
//                String userEmail = rs.getString(6);
//                String userPhoneNumber = rs.getString(7);
//                int userBalance = rs.getInt(8);
//                User user = new User(
//                        userId,
//                        new Location(userLocX, userLocY),
//                        userFirstName,
//                        userLastName,
//                        userEmail,
//                        userPhoneNumber,
//                        userBalance
//                );
////                new CartRepository().findById()
//
////                return rs.getboolean(1);
//            }
//        } catch (SQLException e) {
//            System.out.println("error in UserRepository.userExists query.");
//        }
//        return null;
//    }

    // returns id of newly created User
    public Integer addUser(User user, String password) {
//        String hash = Hashing.hash(password);
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getAddStatement(), Statement.RETURN_GENERATED_KEYS)) {
            st.setString(1, user.getFirstName());
            st.setString(2, user.getLastName());
            st.setString(3, password);
            st.setDouble(4, user.getLocation().getX());
            st.setDouble(5, user.getLocation().getY());
            st.setString(6, user.getEmail());
            st.setString(7, user.getPhoneNumber());
            st.setInt   (8, user.getBalance());
            st.executeUpdate();
            ResultSet rs = st.getGeneratedKeys();
            if (rs.next()){
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println("error in UserRepository.addUser query.");
            e.printStackTrace();
        }
        return null;
    }

    public Integer getUserIdByEmail(String email) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getUserIdByEmailStatement())) {
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println("error in UserRepository.getUserIdByEmail query.");
            e.printStackTrace();
        }
        return null;
    }

    public void increaseBalance(int userId, int amount) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getIncreaseBalanceStatement())) {
            st.setInt(1, amount);
            st.setInt(2, userId);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println("error in UserRepository.updateBalance query.");
            e.printStackTrace();
        }
    }
}
//TODO: close connection, stmt, and rs
