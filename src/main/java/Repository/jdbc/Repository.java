package Repository.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class Repository<T,I> implements IRepository<T,I> {
    abstract protected String getFindStatement();
    abstract protected T convertResultToModel(ResultSet rs) throws SQLException;
    public T findById(I id){
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getFindStatement())) {
            if(id instanceof Integer) {
                st.setInt(1, (Integer) id);
            }
            else {
                st.setString(1, id.toString());
            }
            ResultSet rs;
            try {
                rs = st.executeQuery();
                if (rs.next())
                    return convertResultToModel(rs);
            }
            catch (SQLException e) {
                System.out.println("error in Mapper.findByID query.");
                throw e;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
