package Repository.jdbc.Cart;

import Model.Cart;
import Repository.jdbc.IRepository;

public interface ICartRepository extends IRepository<Cart, Integer> {
    Cart createCart(int userId);
}
