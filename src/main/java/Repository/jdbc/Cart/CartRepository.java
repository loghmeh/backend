package Repository.jdbc.Cart;

import Model.Cart;
import Model.CartFoodItem;
import Model.CartMode;
import Repository.jdbc.Database;
import Repository.jdbc.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class CartRepository extends Repository<Cart, Integer> implements ICartRepository{
    private final String tableName = "carts";
    private final LinkedHashMap<String, Class> cols = new LinkedHashMap<String, Class>() {{
        put("user_id", Integer.class);
        put("rest_id", String.class);
        put("cart_mode", CartMode.class);
    }};

    private String getColumnsSqlString() {
        return String.join(",", cols.keySet());
    }

    private String getAddStatement() {
        return "INSERT INTO " + tableName
                + " (" + getColumnsSqlString() + ") "
                + "VALUES " +
                "(?, ?, ?)";
    }

    private String getUpdateCartStatement() {
        return "UPDATE " + tableName
                + " SET rest_id = ?, cart_mode = ?"
                + " WHERE user_id = ?";
    }

    @Override
    protected String getFindStatement() {
        return "SELECT *"+
                " FROM " + tableName +
                " WHERE user_id = ?";
    }

    @Override
    protected Cart convertResultToModel(ResultSet rs) throws SQLException {
        int userId = rs.getInt(1);
        ArrayList<CartFoodItem> foods = Database.cartFoodItemRepository.findByCartId(userId);
        Cart cart = new Cart();
        cart.setRestaurantId(rs.getString(2));
        cart.setCartMode(CartMode.of(rs.getInt(3)));
        cart.setFoods(foods);
        return cart;
    }

    public Cart createCart(int userId) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getAddStatement())) {
            st.setInt(1, userId);
            st.setString(2, null);
            st.setInt(3, CartMode.EMPTY.getValue());
            st.executeUpdate();
            Cart newCart = new Cart();
            newCart.setRestaurantId(null);
            newCart.setCartMode(CartMode.EMPTY);
            newCart.setFoods(new ArrayList<>());
            return newCart;
        } catch (SQLException e) {
            System.out.println("error in RestaurantRepository.addRestaurant query.");
            e.printStackTrace();
        }
        return null;
    }

    public void updateCart(int userId, String restaurantId, CartMode cartMode) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getUpdateCartStatement())) {
            st.setString(1, restaurantId);
            st.setInt(2, cartMode.getValue());
            st.setInt(3, userId);
            st.executeUpdate();
            //TODO: Does it need returning new empty Cart?
//            Cart newCart = new Cart();
//            newCart.setRestaurantId(null);
//            newCart.setCartMode(CartMode.EMPTY);
//            newCart.setFoods(new ArrayList<>());
//            return newCart;
        } catch (SQLException e) {
            System.out.println("error in RestaurantRepository.addRestaurant query.");
            e.printStackTrace();
        }
//        return null;
    }
}
