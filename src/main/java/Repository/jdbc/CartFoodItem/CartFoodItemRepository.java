package Repository.jdbc.CartFoodItem;

import Model.CartFoodItem;
import Model.Food;
import Repository.jdbc.Database;
import Repository.jdbc.Repository;

import java.sql.*;
import java.util.ArrayList;

public class CartFoodItemRepository extends Repository<CartFoodItem, Integer> implements ICartFoodItemRepository  {

    protected String getIsCartEmptyStatement() {
        return "SELECT COUNT(*) = 0" +
                " FROM cart_items" +
                " WHERE cart_items.cart_id = ?";
    }

    protected String getFindByOrderIdStatement() {
        return "SELECT"+
                    " cart_items.count,"+
                    " cart_items.order_id,"+
                    " foods.id,"+
                    " foods.name,"+
                    " foods.image,"+
                    " foods.description,"+
                    " foods.popularity,"+
                    " foods.price,"+
                    " foods.rest_id"+
                " FROM cart_items"+
                " JOIN foods"+
                    " ON cart_items.food_id = foods.id"+
                " WHERE cart_items.order_id = ?";
    }

    protected String getFindByCartIdStatement() {
        return "SELECT"+
                " cart_items.count,"+
                " cart_items.cart_id,"+
                " foods.id,"+
                " foods.name,"+
                " foods.image,"+
                " foods.description,"+
                " foods.popularity,"+
                " foods.price,"+
                " foods.rest_id"+
                " FROM cart_items"+
                " JOIN foods"+
                " ON cart_items.food_id = foods.id"+
                " WHERE cart_items.cart_id = ?";
    }

    protected String getFindByCartIdAndIdStatement() {
        return "SELECT"+
                " cart_items.count,"+
                " cart_items.cart_id,"+
                " foods.id,"+
                " foods.name,"+
                " foods.image,"+
                " foods.description,"+
                " foods.popularity,"+
                " foods.price,"+
                " foods.rest_id"+
                " FROM cart_items"+
                " JOIN foods"+
                " ON cart_items.food_id = foods.id"+
                " WHERE cart_items.cart_id = ? AND cart_items.food_id = ?";
    }

    @Override
    protected String getFindStatement() {
        return "SELECT"+
                " cart_items.count,"+
                " cart_items.cart_id,"+
                " foods.id,"+
                " foods.name,"+
                " foods.image,"+
                " foods.description,"+
                " foods.popularity,"+
                " foods.price,"+
                " foods.rest_id"+
                " FROM cart_items"+
                " JOIN foods"+
                " ON cart_items.food_id = foods.id"+
                " WHERE cart_items.food_id = ?";
    }

    public String getUpdateByOrderIdStatement(){
        return "UPDATE cart_items"+
                " SET"+
                    " cart_id = ?,"+
                    " order_id = ?"+
                " WHERE cart_id = ?";
    }

    public String getUpdateByCountStatement(){
        return "UPDATE cart_items"+
                " SET"+
                " count = ?"+
                " WHERE cart_id = ? AND food_id = ?";
    }

    public String getAddStatement(){
        return "INSERT INTO cart_items ("
                + " count,"
                + " food_id,"
                + " cart_id,"
                + " order_id ) VALUES ("
                + "?, ?, ?, ?)";
    }

    public String getDeleteFromCartStatement(){
        return "DELETE"+
                " FROM cart_items"+
                " WHERE food_id = ? AND cart_id = ?";
    }

    protected String getExistsByCartIdStatement() {
        return "SELECT"+
                " cart_items.count,"+
                " cart_items.cart_id,"+
                " foods.id,"+
                " foods.name,"+
                " foods.image,"+
                " foods.description,"+
                " foods.popularity,"+
                " foods.price,"+
                " foods.rest_id"+
                " FROM cart_items"+
                " JOIN foods"+
                " ON cart_items.food_id = foods.id"+
                " WHERE cart_items.cart_id = ? AND food.id = ?";
    }

    private String getAnyCartFoodItemExistsInCartStatement() {
        return "SELECT COUNT(*) > 0" +
                " FROM cart_items" +
                " WHERE cart_id = ? AND food_id = ?";
    }

    @Override
    protected CartFoodItem convertResultToModel(ResultSet rs) throws SQLException {
        Food food = new Food(
            rs.getString("id"),
            rs.getString("name"),
            rs.getString("image"),
            rs.getString("description"),
            rs.getFloat("popularity"),
            rs.getInt("price"),
                rs.getString("rest_id")
        );
        return new CartFoodItem(
          food,
                rs.getInt("count")
        );
    }

    @Override
    public ArrayList<CartFoodItem> findByOrderId(int orderId) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getFindByOrderIdStatement())) {
            st.setInt(1, orderId);
            ResultSet rs = null;
            try{
                rs = st.executeQuery();
                ArrayList<CartFoodItem> cartFoodItems = new ArrayList<>();
                while(rs.next()){
                    CartFoodItem cartFoodItem = convertResultToModel(rs);
                    cartFoodItems.add(cartFoodItem);
                }
                return cartFoodItems;
            }catch(SQLException ex){
                System.out.println("error in CartFoodRepository.findByOrderId");
            }finally {
                if(rs != null)
                    rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ArrayList<CartFoodItem> findByCartId(int cartId) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getFindByCartIdStatement())) {
            st.setInt(1, cartId);
            ResultSet rs = null;
            try{
                rs = st.executeQuery();
                ArrayList<CartFoodItem> cartFoodItems = new ArrayList<>();
                while(rs.next()){
                    CartFoodItem cartFoodItem = convertResultToModel(rs);
                    cartFoodItems.add(cartFoodItem);
                }
                return cartFoodItems;
            }catch(SQLException ex){
                System.out.println("error in CartFoodRepository.findByCartId");
            }finally {
                if(rs != null)
                    rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void updateByOrderId(int cartId, int orderId) {
        try(Connection con = Database.getConnection();
        PreparedStatement st = con.prepareStatement(getUpdateByOrderIdStatement())) {
            st.setNull(1, Types.INTEGER);
            st.setInt(2, orderId);
            st.setInt(3, cartId);
            try{
                st.executeUpdate();
            }catch (SQLException ex){
                System.out.println("error in CartFoodRepository.updateByOrderId");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateByCount(CartFoodItem cartFoodItem, int cartId) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getUpdateByCountStatement())) {
            st.setInt(1, cartFoodItem.getCount());
            st.setInt(2, cartId);
            st.setString(3, cartFoodItem.getId());
            try{
                st.executeUpdate();
            }catch (SQLException ex){
                System.out.println("error in CartFoodRepository.updateByCount");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public CartFoodItem findByCartIdAndId(int cartId, String foodId) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getFindByCartIdAndIdStatement())) {
            st.setInt(1, cartId);
            st.setString(2, foodId);
            ResultSet rs = null;
            try{
                rs = st.executeQuery();
                if(rs.next())
                    return convertResultToModel(rs);
            }catch (SQLException ex){
                System.out.println("error in CartFoodRepository.findByCartIdAndId");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void add(CartFoodItem cartFoodItem, int cartId) {
        try(Connection con = Database.getConnection();
        PreparedStatement st = con.prepareStatement(getAddStatement())) {
            st.setInt(1,cartFoodItem.getCount());
            st.setString(2, cartFoodItem.getId());
            st.setInt(3, cartId);
            st.setNull(4, Types.INTEGER);
            try {
                st.executeUpdate();
            }catch(SQLException ex){
                System.out.println("error in CartFoodRepository.add");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isCartEmpty(int cartId){
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getIsCartEmptyStatement())) {
            st.setInt(1, cartId);
            ResultSet rs = null;
            try{
                rs = st.executeQuery();
                if(rs.next()){
                    return rs.getBoolean(1);
                }
                return false;
            }catch(SQLException ex){
                System.out.println("error in CartFoodItemRepository.isCartEmpty");
            }finally {
                if(rs != null)
                    rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void deleteFromCart(String foodId, int userId) {
        try(Connection con = Database.getConnection();
        PreparedStatement st = con.prepareStatement(getDeleteFromCartStatement())) {
            st.setString(1, foodId);
            st.setInt(2, userId);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println("error in CartFoodRepository.delete");
            e.printStackTrace();
        }
    }

    @Override
    public boolean existsByCartId(CartFoodItem cartFoodItem, int cartId) {
        try(Connection con = Database.getConnection();
            PreparedStatement st = con.prepareStatement(getAnyCartFoodItemExistsInCartStatement())) {
            st.setInt(1, cartId);
            st.setString(2, cartFoodItem.getId());
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                return rs.getBoolean(1);
            }
            return false;
        } catch (SQLException e) {
            System.out.println("error in CartFoodItemRepository.existsById");
            e.printStackTrace();
        }
        return false;
    }
}
