package Repository.jdbc.CartFoodItem;

import Model.Cart;
import Model.CartFoodItem;
import Repository.jdbc.IRepository;

import java.util.ArrayList;

public interface ICartFoodItemRepository extends IRepository<CartFoodItem, Integer> {
    ArrayList<CartFoodItem> findByOrderId(int orderId);
    ArrayList<CartFoodItem> findByCartId(int orderId);
    void updateByOrderId(int cartId, int orderId);
    void add(CartFoodItem cartFoodItem, int cartId);
    void deleteFromCart(String foodId, int userId);
    boolean existsByCartId(CartFoodItem cartFoodItem, int cartId);
    void updateByCount(CartFoodItem cartFoodItem, int cartId);
    boolean isCartEmpty(int CartId);
    CartFoodItem findByCartIdAndId(int cartId, String foodId);
}
