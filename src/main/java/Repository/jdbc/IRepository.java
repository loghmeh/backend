package Repository.jdbc;

import java.sql.SQLException;

public interface IRepository<T, I> {

    T findById(I id) throws SQLException;
}
