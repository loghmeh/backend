package Schedulers;

import BusinessLogic.PartyListGetter;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class FoodPartyInvoker implements ServletContextListener {
    private static final int DEFAULT_PERIOD_SEC_MINUTE = 10;
    private static long periodSec = DEFAULT_PERIOD_SEC_MINUTE * 60;
    private ScheduledExecutorService scheduler;

    public static long getPeriodSec() {
        return periodSec;
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
        scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(new PartyListGetter(), 0,  periodSec, TimeUnit.SECONDS);
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        scheduler.shutdownNow();
    }

}
