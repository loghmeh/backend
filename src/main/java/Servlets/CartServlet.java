package Servlets;

import BusinessLogic.AddFoodToCartHandler;
import BusinessLogic.DeleteFoodFromCartHandler;
import BusinessLogic.GetCartCommand;
import DTO.CartDTO;
import DTO.MessageDTO;
import Exceptions.*;
import Model.User;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/api/cart")
public class CartServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer userId = (Integer) request.getAttribute("currentUserId");
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        ObjectMapper mapper = new ObjectMapper();
        Object responseObj;

        CartDTO cart= new GetCartCommand(userId).execute();
        responseObj = cart;
        response.setStatus(200);

        String responseJson = mapper.writeValueAsString(responseObj);
        out.println(responseJson);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer userId = (Integer) request.getAttribute("currentUserId");
        String message;
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(request.getInputStream());
        String foodId, restaurantId;
        Object responseObj = null;

        foodId = node == null
                ? null
                : (node.has("foodId")
                    ? node.get("foodId").asText()
                    : null);
        restaurantId = node == null
                ? null
                : (node.has("restaurantId")
                    ? node.get("restaurantId").asText()
                    : null);

        try {
//            new AddFoodToCartHandler(restaurantId, foodId).execute(currentUser);
            CartDTO cart = new AddFoodToCartHandler(restaurantId, foodId, userId).execute();
            responseObj = cart;
            response.setStatus(200);
            message = "غذا با موفقیت به سبد خرید اضافه شد.";
            response.setStatus(200);
        } catch (NotEnoughParametersException e) {
            message = "لطفاً شناسه‌ی رستوران و غذا را وارد کنید.";
            response.setStatus(400);
        } catch (CartCanOnlyContainFoodsFromOneRestaurantException e) {
            message = "سبد خرید می‌تواند تنها شامل غذاهای یک رستوران باشد.";
            response.setStatus(400);
        } catch (FoodDoesntExistException e) {
            message = "غذایی با این شناسه وجود ندارد.";
            response.setStatus(404);
        } catch (CartCanOnlyContainFoodsFromOneCategoryException e) {
            message = "تنها مجاز به خرید از یک جشنواره هستید. یا فودپارتی یا عادی.";
            response.setStatus(400);
        } catch (PartyFoodFinishedException e) {
            message = "تعداد فودپارتی موجود این غذا به پایان رسیده است.";
            response.setStatus(400);
        } catch (FoodPartyEndedException e) {
            message = "فود پارتی تمام شده است.";
            response.setStatus(400);
        } catch (PartyFoodDoesNotExistsException e) {
            message = "غذایی با این نام در میهمانی غذای این رستوران وجود ندارد.";
            response.setStatus(404);
        }
        if (responseObj == null) {
            responseObj = new MessageDTO(message);
        }
        String responseJson = mapper.writeValueAsString(responseObj);
        out.println(responseJson);
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer userId = (Integer) request.getAttribute("currentUserId");
        String message;
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(request.getInputStream());
        String foodId, restaurantId;
        Object responseObj = null;

        foodId = node == null
                ? null
                : (node.has("foodId")
                ? node.get("foodId").asText()
                : null);
        restaurantId = node == null
                ? null
                : (node.has("restaurantId")
                ? node.get("restaurantId").asText()
                : null);

        try {
            CartDTO cart= new DeleteFoodFromCartHandler(restaurantId, foodId, userId).execute();
            message = "غذا با موفقیت از سبد خرید حذف شد.";
            responseObj = cart;
            response.setStatus(200);
        } catch (NotEnoughParametersException e) {
            message = "لطفاً شناسه‌ی رستوران و غذا را وارد کنید.";
            response.setStatus(400);
        } catch (FoodDoesntExistException e) {
            message = "غذایی با این شناسه وجود ندارد.";
            response.setStatus(404);
        } catch (FoodDoesntExistInCartException e) {
            message = "غذایی با این شناسه در سبد خرید وجود ندارد.";
            response.setStatus(404);
        }
        if(responseObj == null) {
            responseObj = new MessageDTO(message);
        }
        String responseJson = mapper.writeValueAsString(responseObj);
        out.println(responseJson);
    }
}

//TODO: Check for bad JSON requests