package Servlets;

import BusinessLogic.GoogleLoginCommand;
import BusinessLogic.LoginCommand;
import BusinessLogic.RegisterCommand;
import DTO.MessageDTO;
import Exceptions.AuthenticationFailureException;
import Exceptions.BadParametersException;
import Exceptions.NotEnoughParametersException;
import Exceptions.UserWithEmailExistsException;
import Util.JsonUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

@WebServlet("/auth/*")
public class AuthServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json");
        String message;
        PrintWriter out = response.getWriter();
        ObjectMapper mapper = new ObjectMapper();
        Object responseObj;
        JsonNode node = mapper.readTree(request.getInputStream());
        String path = request.getPathInfo();
        switch (path) {
            case "/login": {
                String email = JsonUtil.getValueFromJsonRequest("email", node);
                String password = JsonUtil.getValueFromJsonRequest("password", node);
                try {
                    String jwt = new LoginCommand(email, password).execute();
                    HashMap<String, Object> responseMap = new HashMap<String, Object>() {{
                        put("JWT", jwt);
                    }};
                    responseObj = responseMap;
                    response.setStatus(HttpServletResponse.SC_OK);
                } catch (NotEnoughParametersException e) {
                    message = "پارامترهای وارد شده صحیح نمی‌باشند.";
                    responseObj = new MessageDTO(message);
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                } catch (AuthenticationFailureException e) {
                    message = "ایمیل یا رمز عبور صحیح نمی‌باشد.";
                    responseObj = new MessageDTO(message);
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED); //TODO: Is this a correct response code?
                }
                break;
            }
            case "/google":
                String accessToken = JsonUtil.getValueFromJsonRequest("idToken", node);
                try {
                    String jwt = new GoogleLoginCommand(accessToken).execute();
                    HashMap<String, Object> responseMap = new HashMap<String, Object>() {{
                        put("JWT", jwt);
                    }};
                    responseObj = responseMap;
                    response.setStatus(HttpServletResponse.SC_OK);
                } catch (AuthenticationFailureException e) {
                    message = "توکن ارسالی معتبر نمی‌باشد.";
                    responseObj = new MessageDTO(message);
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                } catch (BadParametersException e) {
                    message = "پارامترهای وارد شده صحیح نمی‌باشند.";
                    responseObj = new MessageDTO(message);
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                }
                break;
            case "/register": {
                String email = JsonUtil.getValueFromJsonRequest("email", node);
                String firstName = JsonUtil.getValueFromJsonRequest("firstName", node);
                String lastName = JsonUtil.getValueFromJsonRequest("lastName", node);
                String password = JsonUtil.getValueFromJsonRequest("password", node);
                String phoneNumber = JsonUtil.getValueFromJsonRequest("phoneNumber", node);
                try {
                    new RegisterCommand(email, firstName, lastName, password, phoneNumber).execute();
                    message = "ثبت نام شما با موفقیت انجام شد.";
                    responseObj = new MessageDTO(message);
                    response.setStatus(200);
                } catch (UserWithEmailExistsException e) {
                    message = "ایمیل وارد شده تکراری است.";
                    responseObj = new MessageDTO(message);
                    response.setStatus(401); //TODO: Is this a correct response code?
                } catch (BadParametersException e) {
                    message = "پارامترهای وارد شده صحیح نمی‌باشند.";
                    responseObj = new MessageDTO(message);
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                }
                break;
            }
            default:
                response.setStatus(404);
                return;
        }

        String responseJson = mapper.writeValueAsString(responseObj);
        out.println(responseJson);

    }
}
