package Servlets;

import BusinessLogic.ChargeAccountCommand;
import BusinessLogic.ViewUserProfileCommand;
import DTO.MessageDTO;
import Exceptions.NotEnoughParametersException;
import Model.User;
import Exceptions.NegativeOrZeroChargeAmountException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/api/user/balance")
public class UserBalanceServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer userId = (Integer) request.getAttribute("currentUserId");
//        User currentUser = (User) request.getAttribute("currentUser");
        String message;
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(request.getInputStream());
        Object responseObj;

        Integer amount = node == null
                ? null
                : (node.has("amount")
                    ? Integer.parseInt(node.get("amount").asText())
                    : null);
        try {
            new ChargeAccountCommand(amount, userId).execute();
            message = "اعتبار با موفقیت افزوده شد.";
            response.setStatus(200);
        } catch (NegativeOrZeroChargeAmountException e) {
            message = "مبلغ افزایش اعتبار باید مثبت باشد.";
            response.setStatus(400);
        } catch (NotEnoughParametersException e) {
            message = "وارد کردن مبلغ اقزایش اعتبار ضروری است.";
            response.setStatus(400);
        }
        responseObj = new MessageDTO(message);
        String responseJson = mapper.writeValueAsString(responseObj);
        out.println(responseJson);
    }
}
