package Servlets;

import BusinessLogic.ViewUserProfileCommand;
import DTO.ProfileDTO;
import Model.User;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/api/user/profile")
public class UserProfileServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer userId = (Integer) request.getAttribute("currentUserId");
//        User currentUser = (User) request.getAttribute("currentUser");
        ProfileDTO user = new ViewUserProfileCommand(userId).execute();
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        ObjectMapper mapper = new ObjectMapper();
        String responseJson = mapper.writeValueAsString(user);
        response.setStatus(200);
        out.println(responseJson);
    }
}
