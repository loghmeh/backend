package Servlets;

import BusinessLogic.FinalizeOrderCommand;
import BusinessLogic.ViewOrderCommand;
import DTO.MessageDTO;
import Exceptions.*;
import Model.Order;
import Model.User;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/api/order")
public class OrderServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        Integer userId = (Integer) request.getAttribute("currentUserId");
//        User currentUser = (User) request.getAttribute("currentUser");
        Integer orderId = request.getParameter("id") == null
                ? null
                : Integer.parseInt(request.getParameter("id"));
        String message;
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        ObjectMapper mapper = new ObjectMapper();
        Object responseObj;

        try {
            Order order = new ViewOrderCommand(orderId).execute();
            responseObj = order;
            response.setStatus(200);
        } catch (NotEnoughParametersException e) {
            message = "وارد کردن شناسه‌ی سفارش اجباری است.";
            responseObj = new MessageDTO(message);
            response.setStatus(400);
        } catch (OrderNotFoundException e) {
            message = "سفارشی با این شناسه یافت نشد.";
            responseObj = new MessageDTO(message);
            response.setStatus(404);
        }
        String responseJson = mapper.writeValueAsString(responseObj);
        out.println(responseJson);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer userId = (Integer) request.getAttribute("currentUserId");
//        User currentUser = (User) request.getAttribute("currentUser");
        String message;
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        ObjectMapper mapper = new ObjectMapper();
        Object responseObj;

        try {
            new FinalizeOrderCommand(userId).execute();
            message = "سفارش شما با موفقیت ثبت شد.";
            response.setStatus(200);
        } catch (CartIsEmptyException e) {
            message = "سبد خرید خالی است!";
            response.setStatus(400);
        } catch (InsufficientBalanceToFinalizeOrderException e) {
            message = "اعتبار کافی برای تکمیل سغارش را ندارید.";
            response.setStatus(402);
        }

        responseObj = new MessageDTO(message);
        String responseJson = mapper.writeValueAsString(responseObj);
        out.println(responseJson);
    }
}
