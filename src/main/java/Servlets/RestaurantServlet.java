package Servlets;

import BusinessLogic.ViewAvailableRestaurantsCommand;
import BusinessLogic.ViewRestaurantInfoCommand;
import DTO.MessageDTO;
import DTO.RestaurantDTO;
import DTO.RestaurantMetadataDTO;
import DTO.RestaurantWithEtaDTO;
import Data.Manager.UserManager;
import Exceptions.*;
import Model.Restaurant;
import Model.User;
import Util.Time;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

@WebServlet("/api/restaurants")
public class RestaurantServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer userId = (Integer) request.getAttribute("currentUserId");
//        User currentUser = (User) request.getAttribute("currentUser");
        String restaurantId = request.getParameter("id");
        String message;
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        ObjectMapper mapper = new ObjectMapper();
        Object responseObj;

        if (restaurantId == null) {
            String pageIdStr = request.getParameter("pageId");
            String pageChunkStr = request.getParameter("pageChunk");

            try {

                ArrayList<Restaurant> availableRestaurants;
                if (pageIdStr == null || pageChunkStr == null) {
                    availableRestaurants = new ViewAvailableRestaurantsCommand().execute();
                } else {
                    availableRestaurants = new ViewAvailableRestaurantsCommand(pageIdStr, pageChunkStr).execute();
                }
                ArrayList<RestaurantMetadataDTO> restaurants = new ArrayList<>();
                for (Restaurant restaurant: availableRestaurants) {
                    restaurants.add(new RestaurantMetadataDTO(restaurant));
                }
                HashMap<String, Object> responseMap = new HashMap<>();
                responseMap.put("restaurants", restaurants);
                responseObj = responseMap;
                response.setStatus(200);
            } catch (NoNearbyRestaurantAvailableException e) {
                message = "رستوراتی در اطراف شما پیدا نشد.";
                responseObj = new MessageDTO(message);
                response.setStatus(404);
            } catch (BadParametersException e) {
                message = "پارامترهای وارد شده صحیح نمی‌باشند.";
                responseObj = new MessageDTO(message);
                response.setStatus(400);
            }
        }
        else {
            try {
                Restaurant restaurant = new ViewRestaurantInfoCommand(restaurantId, userId).execute();
                RestaurantDTO restaurantDTO = new RestaurantDTO(restaurant);
                String estimatedEta = Time.dateToString(
                        Time.estimateEta(UserManager.getUserLocation(userId), restaurant.getLocation())
                );
                responseObj = new RestaurantWithEtaDTO(restaurantDTO, estimatedEta);
                response.setStatus(200);
            } catch (NotEnoughParametersException e) {
                message = "لطفاً شناسه‌ی رستوران را وارد کنید.";
                responseObj = new MessageDTO(message);
                response.setStatus(400);
            } catch (RestaurantTooFarException e) {
                message = "رستوران بسیار دورتر از شماست.";
                responseObj = new MessageDTO(message);
                response.setStatus(400);
            } catch (RestaurantDoesntExistException e) {
                message = "رستورانی با این شناسه یافت نشد.";
                responseObj = new MessageDTO(message);
                response.setStatus(404);
            }
        }

        String responseJson = mapper.writeValueAsString(responseObj);
        out.println(responseJson);

    }
}
