package Servlets;


import BusinessLogic.GetRestaurantSearchResultsCommand;
import DTO.MessageDTO;
import DTO.RestaurantListDTO;
import DTO.RestaurantMetadataDTO;
import Model.Restaurant;
import Model.User;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;


@WebServlet("/api/search")
public class SearchServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        Integer userId = (Integer) request.getAttribute("currentUserId");
//        User user = (User) request.getAttribute("currentUser");
        ObjectMapper mapper = new ObjectMapper();
        String message;
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        JsonNode node = mapper.readTree(request.getInputStream());
        String foodSearch = node == null
                ? null
                : (node.has("foodSearch")
                ? node.get("foodSearch").asText()
                : null);
        String restaurantSearch = node == null
                ? null
                : (node.has("restaurantSearch")
                ? node.get("restaurantSearch").asText()
                : null);

        Object responseObj;
        try {
            ArrayList<Restaurant> foundRestaurants =
                    new GetRestaurantSearchResultsCommand(foodSearch, restaurantSearch).execute();
//            if(foundRestaurants.isEmpty()){
//                message = "رستورانی یافت نشد.";
//                responseObj = new MessageDTO(message);
//                response.setStatus(200);
//            }
//            else{
                HashMap<String, Object> responseMap = new HashMap<>();
                ArrayList<RestaurantMetadataDTO> restaurants = new ArrayList<>();
                for (Restaurant restaurant: foundRestaurants) {
                    restaurants.add(new RestaurantMetadataDTO(restaurant));
                }
                responseMap.put("restaurants",restaurants);
                responseObj = responseMap;
                response.setStatus(200);
//            }

        } catch (Exception e) {
            message = " جست و جو با مشکل مواجه شد.";
            responseObj = new MessageDTO(message);
            response.setStatus(400);
        }

        String responseJson = mapper.writeValueAsString(responseObj);
        out.println(responseJson);

    }
}
