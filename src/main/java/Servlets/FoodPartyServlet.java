package Servlets;

import BusinessLogic.ViewFoodPartyRestaurantsCommand;
import DTO.FoodPartyRestaurantDTO;
import DTO.MessageDTO;
import Data.Access.DatabaseAccess;
import Data.Manager.LoghmehManager;
import Exceptions.NoNearbyRestaurantAvailableException;
import Model.User;
import Util.Time;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

@WebServlet("/api/foodparty")
public class FoodPartyServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        Integer userId = (Integer) request.getAttribute("currentUserId");
//        User currentUser = (User) request.getAttribute("currentUser");
        String message;
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        ObjectMapper mapper = new ObjectMapper();
        Object responseObj;
        try {
            ArrayList<FoodPartyRestaurantDTO> partyRestaurants = new ViewFoodPartyRestaurantsCommand().execute();
            String foodPartyFinishTime = Time.dateToString(
                    LoghmehManager.getFoodPartyEndTime()
            );
            HashMap<String, Object> responseMap = new HashMap<>();
            responseMap.put("restaurants", partyRestaurants);
            responseMap.put("finishTime", foodPartyFinishTime);
            responseObj = responseMap;
            response.setStatus(200);
        } catch (NoNearbyRestaurantAvailableException e) {
            message = "رستوراتی در اطراف شما پیدا نشد.";
            responseObj = new MessageDTO(message);
            response.setStatus(404);
        }
        String responseJson = mapper.writeValueAsString(responseObj);
        out.println(responseJson);
    }
}
