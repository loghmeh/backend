package Commands.Response;

public class RestaurantNameResponse {
    private String name;

    public RestaurantNameResponse(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
