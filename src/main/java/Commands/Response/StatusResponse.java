package Commands.Response;

public class StatusResponse {
    private int status;
    private String message;

    public StatusResponse(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
