package Commands.Request;

import Model.Food;
import Model.Location;

import java.net.URL;
import java.util.ArrayList;

public class AddRestaurantRequest {
    private String name;
    private String id;
    private URL logo;
    private Location location;
    private ArrayList<Food> menu;

    public String getName() {
        return name;
    }


    public String getId() {
        return id;
    }

    public URL getLogo() {
        return logo;
    }


    public Location getLocation() {
        return location;
    }

    public ArrayList<Food> getMenu() {
        return menu;
    }
}
