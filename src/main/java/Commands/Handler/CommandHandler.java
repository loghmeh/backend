package Commands.Handler;

import Commands.Request.AddRestaurantRequest;
import Commands.Response.StatusResponse;
import Data.Access.DatabaseAccess;
import Data.Access.UserSessionAccess;
import Model.Courier;
import Model.Food;
import Model.Restaurant;
import Exceptions.*;
import Services.CourierList;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static Util.Math.*;

public class CommandHandler {
//    public static List<Courier>  getCouriersHandler(){
//        CourierList courierList = new CourierList();
//        String jsonData = courierList.sendGet();
//        ArrayList<Courier> couriers = new ArrayList<>();
//        try{
//            couriers = new ObjectMapper().readValue(jsonData, new TypeReference<List<Courier>>(){});
//        } catch (JsonParseException e) {
//            e.printStackTrace();
//        } catch (JsonMappingException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return couriers;
//    }
//    public static Object addRestaurantsHandler(String jsonData) {
//        if (jsonData == null) {
//            return new StatusResponse(400, "Request data is mandatory.");
//        }
//        List<AddRestaurantRequest> restaurantsReqs;
//        try {
//            restaurantsReqs = Arrays.asList(new ObjectMapper().readValue(jsonData, AddRestaurantRequest[].class));
//        } catch (IOException e) {
//            return new StatusResponse(500, "JSON Parse Error");
//        }
//        try {
//            List<Restaurant> restaurants = new ArrayList<Restaurant>();
//            for(AddRestaurantRequest req: restaurantsReqs){
//                restaurants.add(new Restaurant(req));
//            }
//            DatabaseAccess.addRestaurants(restaurants);
//        } catch (RestaurantAlreadyExistsException e) {
//            return new StatusResponse(401, "Restaurant already exists.");
//        }
//        return new StatusResponse(200, "Restaurant added successfully.");
//
//    }
//    public static Object addRestaurantHandler(String jsonData) {
//        if (jsonData == null) {
//            return new StatusResponse(400, "Request data is mandatory.");
//        }
//        Restaurant restaurant;
//        try {
//            restaurant = new Restaurant(new ObjectMapper().readValue(jsonData, AddRestaurantRequest.class));
//        } catch (IOException e) {
//            return new StatusResponse(500, "JSON Parse Error");
//        }
//        try {
//            DatabaseAccess.addRestaurant(restaurant);
//        } catch (RestaurantAlreadyExistsException e) {
//            return new StatusResponse(401, "Restaurant already exists.");
//        }
//        return new StatusResponse(200, "Restaurant added successfully.");
//    }

//    public static Object addFoodHandler(String jsonData) {
//        if (jsonData == null) {
//            return new StatusResponse(400, "Request data is mandatory.");
//        }
//        Food food;
//        String restaurantName;
//        try {
//            ObjectMapper objectMapper = new ObjectMapper();
//            food = objectMapper.readValue(jsonData, Food.class);
//            restaurantName = objectMapper.readTree(jsonData).get("restaurantName").asText();
//        } catch (IOException e) {
//            return new StatusResponse(500, "JSON Parse Error");
//        } catch (NullPointerException e) {
//            return new StatusResponse(400, "Bad Request");
//        }
//        try {
//            DatabaseAccess.addFood(food, restaurantName);
//        } catch (FoodAlreadyExistsException e) {
//            return new StatusResponse(400, "Food already exists in this restaurant.");
//        } catch (RestaurantDoesntExistException e) {
//            return new StatusResponse(400, "Restaurant doesn't exist.");
//        }
//        return new StatusResponse(200, "Food added successfully.");
//    }

//    public static Object getRestaurantsHandler(String jsonData) {
//        if (jsonData != null) {
//            return new StatusResponse(400, "Request doesn't accept any arguments.");
//        }
//        return DatabaseAccess.getRestaurantNames();
//    }

//    public static Object getRestaurantHandler(String jsonData) {
//        if (jsonData == null) {
//            return new StatusResponse(400, "Request data is mandatory.");
//        }
//        String restaurantName;
//        try {
//            restaurantName = new ObjectMapper().readTree(jsonData).get("restaurantName").asText();
//        } catch (IOException e) {
//            return new StatusResponse(500, "JSON Parse Error");
//        } catch (NullPointerException e) {
//            return new StatusResponse(400, "Bad Request");
//        }
//        try {
//            Restaurant restaurant = DatabaseAccess.getRestaurant(restaurantName);
//            if(getRestaurantDistanceToUser(restaurant) > 170) {
//                return new StatusResponse(403, "Restaurant is out of range of service to user.");
//            }
//            return restaurant;
//        } catch (RestaurantDoesntExistException e) {
//            return new StatusResponse(404, "Restaurant doesn't exist.");
//        }
//    }

//    public static Object getFoodHandler(String jsonData) {
//        if (jsonData == null) {
//            return new StatusResponse(400, "Request data is mandatory.");
//        }
//        String restaurantName, foodName;
//        try {
//            ObjectMapper objectMapper = new ObjectMapper();
//            restaurantName = objectMapper.readTree(jsonData).get("restaurantName").asText();
//            foodName = objectMapper.readTree(jsonData).get("foodName").asText();
//        } catch (IOException e) {
//            return new StatusResponse(500, "JSON Parse Error");
//        } catch (NullPointerException e) {
//            return new StatusResponse(400, "Bad Request");
//        }
//        try {
//            return DatabaseAccess.getFood(restaurantName, foodName);
//        } catch (RestaurantDoesntExistException e) {
//            return new StatusResponse(400, "Restaurant doesn't exist.");
//        } catch (FoodDoesntExistException e) {
//            return new StatusResponse(400, "Food doesn't exist in this restaurant.");
//        }
//    }
//    public static Object addToCartHandler(String jsonData) {
//        if (jsonData == null) {
//            return new StatusResponse(400, "Request data is mandatory.");
//        }
//        String restaurantName, foodName;
//        try {
//            ObjectMapper objectMapper = new ObjectMapper();
//            foodName = objectMapper.readTree(jsonData).get("foodName").asText();
//            restaurantName = objectMapper.readTree(jsonData).get("restaurantName").asText();
//        } catch (IOException e) {
//            return new StatusResponse(500, "JSON Parse Error");
//        } catch (NullPointerException e) {
//            return new StatusResponse(400, "Bad Request");
//        }
//        try {
//            UserSessionAccess.addFoodToCart(restaurantName, foodName);
//        } catch (RestaurantDoesntExistException e) {
//            return new StatusResponse(400, "Restaurant doesn't exist.");
//        } catch (FoodDoesntExistException e) {
//            return new StatusResponse(400, "Food doesn't exist in this restaurant.");
//        }catch (CartCanOnlyContainFoodsFromOneRestaurantException e) {
//            return new StatusResponse(400, "You can order from one restaurant");
//        }
//        return new StatusResponse(200, "Food added successfully to cart.");
//    }

//    public static Object getCartHandler(String jsonData) {
//        if (jsonData != null) {
//            return new StatusResponse(400, "Request doesn't accept any arguments.");
//        }
//        return UserSessionAccess.getUserCartFoodItems();
//    }

//    public static Object finalizeOrderHandler(String jsonData) {
//        if (jsonData != null) {
//            return new StatusResponse(400, "Request doesn't accept any arguments.");
//        }
//        //TODO: check balance and emptiness of cart
//        return UserSessionAccess.finalizeUserOrder();
//    }

//    public static Object getRecommendedRestaurantsHandler(String jsonData) {
//        if (jsonData != null) {
//            return new StatusResponse(400, "Request doesn't accept any arguments.");
//        }
//        HashMap<String, Double> restaurantRecommendationPoints = new HashMap<>();
//        for (Restaurant restaurant : DatabaseAccess.getRestaurants()) {
//            restaurantRecommendationPoints.put(
//                    restaurant.getName(), getRestaurantRecommendationPointToUser(restaurant)
//            );
//        }
//        return getTopNKeysByValues(restaurantRecommendationPoints, 3);
//    }
}
