package Util.Hash;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.mindrot.jbcrypt.BCrypt;

import java.util.function.Function;

import static org.mindrot.jbcrypt.BCrypt.gensalt;

public class UpdatableBCrypt {

    private static final Logger log = LoggerFactory.getLogger(UpdatableBCrypt.class);
    private final int logRounds;

    public UpdatableBCrypt(int logRounds) {
        this.logRounds = logRounds;
    }

    public String hash(String plainText) {
        return BCrypt.hashpw(plainText, gensalt(logRounds));
    }

    public boolean verifyHash(String plainText, String hash) {
        return BCrypt.checkpw(plainText, hash);
    }

    //this function is an updating process in database
    public boolean verifyAndUpdateHash(String plainText, String hash, Function<String, Boolean> updateFunc) {
        if(BCrypt.checkpw(plainText, hash)) {
            int rounds = getRounds(hash);
            if (rounds != logRounds) {
                log.debug("Updating password from {} rounds to {}", rounds, logRounds);
                String newHash = hash(plainText);
                return updateFunc.apply(newHash);
            }
            return true;
        }
        return false;
    }

    private int getRounds(String salt){
        char minor = (char)0;
        int off = 0;

        if (salt.charAt(0) != '$' || salt.charAt(1) != '2')
            throw new IllegalArgumentException ("Invalid salt version");
        if (salt.charAt(2) == '$')
            off = 3;
        else {
            minor = salt.charAt(2);
            if (minor != 'a' || salt.charAt(3) != '$')
                throw new IllegalArgumentException ("Invalid salt revision");
            off = 4;
        }

        if (salt.charAt(off + 2) > '$')
            throw new IllegalArgumentException ("Missing salt rounds");
        return Integer.parseInt(salt.substring(off, off + 2));
    }
}

