package Util;


import Model.Location;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static java.lang.Math.sqrt;

public class Time {
    public static double calculateEta(
            Location restaurantLocation,
            Location courierLocation,
            Location userLocation,
            int courierVelocity) {
        double dist = 0;
        dist += java.lang.Math.hypot(
                restaurantLocation.getY() - courierLocation.getY(),
                restaurantLocation.getX() - courierLocation.getX()
        );
        dist += java.lang.Math.hypot(
                restaurantLocation.getY() - userLocation.getY(),
                restaurantLocation.getX() - userLocation.getX()
        );
        return dist / courierVelocity;
    }

    public static LocalDateTime estimateEta(Location loc1, Location loc2){
        double dist = 0;
        dist += sqrt(
                ((loc1.getX() - loc2.getX()) * (loc1.getX() - loc2.getX()))
                        + ((loc1.getY() - loc2.getY()) * (loc1.getY() - loc2.getY()))
        );
        dist += dist / 2;
        double eta = dist / 5 + 60;
        return LocalDateTime.now().plusSeconds((long)eta);
    }

    public static String dateToString(LocalDateTime dateTime) {
        return DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss").format(dateTime);
    }

    public static LocalDateTime stringToDate(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(dateString, formatter);
        return dateTime;
    }
}
