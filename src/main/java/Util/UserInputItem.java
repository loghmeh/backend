package Util;

public class UserInputItem {
    private String command;
    private String jsonData;

    public UserInputItem(String command, String jsonData) {
        this.command = command;
        this.jsonData = jsonData;
    }

    public String getCommand() {
        return command;
    }

    public String getJsonData() {
        return jsonData;
    }
}
