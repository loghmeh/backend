package Util.auth;

import Config.Loghmeh;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;

public class JWT {
    public static String generateJwtFor(int userId) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        String jwt = Jwts.builder()
                .setIssuer("loghmeh")
                .setIssuedAt(new Date())
                .setExpiration(cal.getTime())
                .claim("userId", userId)
                .signWith(Loghmeh.PRIVATE_KEY)
                .compact();
        return jwt;
    }

    public static String getJWTFromRequest(HttpServletRequest request) {
        String authHeader = request.getHeader(Loghmeh.AUTH_HEADER_KEY);
        if (authHeader != null && authHeader.startsWith(Loghmeh.AUTH_HEADER_VALUE_PREFIX)) {
            return authHeader.substring(Loghmeh.AUTH_HEADER_VALUE_PREFIX.length());
        }
        return null;
    }

    public static Integer getUserIdFromJWT(String jwt) {
        try {
            Jws<Claims> jws = Jwts.parser()
                    .setSigningKey(Loghmeh.PRIVATE_KEY)
                    .parseClaimsJws(jwt);
            return Integer.parseInt(String.valueOf(jws.getBody().get("userId")));
        } catch (Exception e) {
            return null;
        }
    }
}
