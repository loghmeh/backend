package Util;

import Data.Access.UserSessionAccess;
import Model.Food;
import Model.Location;
import Model.Restaurant;

import java.util.*;
import java.util.stream.Collectors;

public class Math {

    public static double euclideanDistance(Location a, Location b) {
        return java.lang.Math.sqrt(
                java.lang.Math.pow(a.getX() - b.getX(), 2) +
                        java.lang.Math.pow(a.getY() - b.getY(), 2)
        );
    }

    public static double getRestaurantRecommendationPointToUser(Restaurant restaurant) {
        ArrayList<Food> foods = new ArrayList<>(restaurant.getMenu());
        double sumFoodsPopularity = 0, avgFoodsPopularity;
        for (Food food : foods) {
            sumFoodsPopularity += food.getPopularity();
        }
        avgFoodsPopularity = foods.size() == 0 ? 0 : sumFoodsPopularity / foods.size();
        double point = avgFoodsPopularity / euclideanDistance(UserSessionAccess.getUserLocation(), restaurant.getLocation());
        return avgFoodsPopularity / euclideanDistance(UserSessionAccess.getUserLocation(), restaurant.getLocation());

    }

    public static ArrayList<String> getTopNKeysByValues(HashMap<String, Double> map, Integer n) {
        HashMap<String, Double> sortedMap = map.entrySet().
                stream().
                sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).
                collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        ArrayList<String> keys = new ArrayList<>(sortedMap.keySet());
        int upperBound = n > keys.size() ? keys.size() : n;
        return new ArrayList<>(keys.subList(0, upperBound));
    }

    public static Double getRestaurantDistanceToUser(Restaurant restaurant) {
        return euclideanDistance(restaurant.getLocation(), UserSessionAccess.getUserLocation());
    }
}
