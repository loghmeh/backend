package Util;

import Model.CartFoodItem;

import java.util.ArrayList;

public class FoodUtil {
    public static boolean isPartyFood(String foodId) {
        return foodId.endsWith("_p");
    }

    public static int findTotalPrice(ArrayList<CartFoodItem> foods) {
        int result = 0;
        for (CartFoodItem cartFoodItem: foods) {
            result += cartFoodItem.getPrice() * cartFoodItem.getCount();
        }
        return result;
    }
}
