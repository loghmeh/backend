package Util;

public class Path {
    public static class Web{
        public static final String RESTAURANTS = "/restaurants";
        public static final String RESTAURANT = "/restaurant/:id";
        public static final String PROFILE = "/profile";
        public static final String CHARGE = "/profile/charge";
        public static final String ADDFOOD = "/restaurant/addfood";
        public static final String FOODLIST = "/cart";
        public static final String CHECKOUT = "/cart/checkout";

    }
    public static class Template{
        public static final String CART = "/velocity/cart.vm";
        public static final String REST_ONE = "/velocity/restaurant/one.vm";
        public static final String REST_ALL = "/velocity/restaurant/all.vm";
        public static final String USER = "/velocity/user.vm";
    }
}
