package Filters;

import Config.Loghmeh;
import Util.auth.JWT;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(asyncSupported = true, urlPatterns = { "/*" })
public class AuthenticationFilter implements Filter {

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        if (!request.getRequestURI().contains("/auth") && !request.getMethod().equals("OPTIONS")) {
            String jwt = JWT.getJWTFromRequest(request);
            Integer userId = JWT.getUserIdFromJWT(jwt);
            if (userId != null) {
                request.setAttribute("currentUserId", userId);
            }
            else {
                resp.addHeader("WWW-Authenticate", Loghmeh.AUTH_HEADER_VALUE_PREFIX);
                resp.setStatus(401);
                return;
            }
        }
        chain.doFilter(request, resp);
    }

    @Override
    public void destroy() {
    }
}