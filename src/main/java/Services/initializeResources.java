package Services;

import Commands.Request.AddRestaurantRequest;
import Data.Access.DatabaseAccess;
import Data.Manager.RestaurantManager;
import Data.Manager.UserManager;
import Exceptions.UserNotFoundException;
import Exceptions.UserWithEmailExistsException;
import Model.Database;
import Model.Location;
import Model.Restaurant;
import Exceptions.RestaurantAlreadyExistsException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

    public class initializeResources implements ServletContextListener,
        HttpSessionListener, HttpSessionAttributeListener {

    public initializeResources() {
        try {
            UserManager.getUserById(1);
        } catch (UserNotFoundException e) {
            try {
                UserManager.addUser(
                        "سید سینا",
                        "خامس‌پناه",
                        "123456",
                        "seyed@sina@nabavi",
                        "09123456789",
                        new Location(0, 0)
                );
            } catch (UserWithEmailExistsException ex) {
                // not happens
            }
        }


        ArrayList<AddRestaurantRequest> restaurants = new ArrayList<>();
        try {
            URL jsonUrl = new URL("http://138.197.181.131:8080/restaurants");
            restaurants = new ObjectMapper().readValue(jsonUrl, new TypeReference<List<AddRestaurantRequest>>(){});
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (AddRestaurantRequest restaurant: restaurants) {
            try {
                //TODO: Diff between AddRestaurantRequest and Restaurant?
                RestaurantManager.addRestaurant(new Restaurant(restaurant));
            } catch (RestaurantAlreadyExistsException e) {
                System.out.println("Duplicate restaurant in initializing. Ignoring this one and keeping the first.");
            }
        }
    }
}
