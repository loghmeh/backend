package Services;

import Model.Courier;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ExternalAPIs {
    public static ArrayList<Courier> getAvailableCouriers() {
        ArrayList<Courier> couriers = new ArrayList<>();
        try {
            URL jsonUrl = new URL("http://138.197.181.131:8080/deliveries");
            couriers = new ObjectMapper().readValue(jsonUrl, new TypeReference<List<Courier>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return couriers;
    }
}
