package Services;

//import DTO.AddFoodPartyRestaurantRequest;
import DTO.FoodPartyRestaurantDTO;
import Data.Access.DatabaseAccess;
import Data.Manager.FoodManager;
import Data.Manager.PartyFoodManager;
import Data.Manager.RestaurantManager;
import Exceptions.FoodAlreadyExistsException;
import Exceptions.PartyFoodExistsException;
import Exceptions.RestaurantAlreadyExistsException;
import Exceptions.RestaurantDoesntExistException;
import Model.PartyFood;
import Model.Restaurant;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FoodPartyList {
    public void sendGet(){
        ArrayList<FoodPartyRestaurantDTO> restaurants = new ArrayList<>();
        try {
            URL jsonUrl = new URL("http://138.197.181.131:8080/foodparty");
            restaurants = new ObjectMapper().readValue(jsonUrl, new TypeReference<List<FoodPartyRestaurantDTO>>(){});
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (FoodPartyRestaurantDTO partyRestaurant: restaurants) {
            Restaurant actualRestaurant = new Restaurant(partyRestaurant);
            try {
                RestaurantManager.addRestaurant(actualRestaurant);
            } catch (RestaurantAlreadyExistsException e) {
                for (PartyFood partyFood: partyRestaurant.getMenu()){
                    actualRestaurant.setFoodId(partyFood);
                    partyFood.setRestaurantId(partyRestaurant.getId());
                    try {
                        PartyFoodManager.addPartyFood(partyFood);
                    } catch (FoodAlreadyExistsException ex) {
                        ex.printStackTrace();
                        //TODO: If happens?
                    }
                }
            }
        }

    }
}
