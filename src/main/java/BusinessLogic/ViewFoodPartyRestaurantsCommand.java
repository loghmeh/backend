package BusinessLogic;

import DTO.FoodPartyRestaurantDTO;
import Data.Access.DatabaseAccess;
import Exceptions.BadParametersException;
import Exceptions.NoNearbyRestaurantAvailableException;
import Model.PartyFood;
import Model.PartyStatus;
import Model.Restaurant;
import Model.User;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

public class ViewFoodPartyRestaurantsCommand implements Command {
    @Override
    public ArrayList<FoodPartyRestaurantDTO> execute() throws NoNearbyRestaurantAvailableException {
        ArrayList<Restaurant> availableRestaurants = null;
        try {
            availableRestaurants = new ViewAvailableRestaurantsCommand().execute();
        } catch (BadParametersException e) {
            // ignore
        }
        ArrayList<FoodPartyRestaurantDTO> partyRestaurants = new ArrayList<>();

        for (Restaurant restaurant: Objects.requireNonNull(availableRestaurants)) {
            ArrayList<PartyFood> restaurantPartyFoods = new ArrayList<>();
            if(restaurant.getPartyMenu().isEmpty()){
                continue;
            }
            for(PartyFood partyFood: restaurant.getPartyMenu()){
                if(partyFood.getStatus().equals(PartyStatus.ACTIVE)) {
                    restaurantPartyFoods.add(partyFood);
                }
            }
            if(restaurantPartyFoods.size() == 0){
                continue;
            }
            partyRestaurants.add(new FoodPartyRestaurantDTO(restaurant, restaurantPartyFoods));
        }
        if (partyRestaurants.size() == 0) {
            throw new NoNearbyRestaurantAvailableException();
        }
        return partyRestaurants;

//        ArrayList<Restaurant> partyRestaurants = new ViewAvailableRestaurantsCommand().execute(currentUser);
//        Iterator<Restaurant> itr = availableRestaurants.iterator();
//
//        while(itr.hasNext()){
//            Restaurant restaurant = itr.next();
//            if(restaurant.getPartyMenu().isEmpty()){
//                itr.remove();
//                continue;
//            }
//            int counter = 0;
//            for(PartyFood partyFood: restaurant.getPartyMenu().values()){
//                if(partyFood.getStatus().equals(PartyStatus.ACTIVE))
//                    break;
//                counter++;
//            }
//            if(counter == restaurant.getPartyMenu().size()){
//                itr.remove();
//            }
//
//        }
//
//        return availableRestaurants;
    }
}
