package BusinessLogic;

import DTO.CartDTO;
import Data.Manager.CartManager;
import Exceptions.CartIsEmptyException;
import Model.Cart;
import Model.User;

public class GetCartCommand implements Command {

    private Integer userId;

    public GetCartCommand(Integer userId) {
        this.userId = userId;
    }

    @Override
    public CartDTO execute() {
        Cart cart = CartManager.findById(userId);
        return new CartDTO(cart);
    }
}
