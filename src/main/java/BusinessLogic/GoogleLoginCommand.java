package BusinessLogic;
import Config.Loghmeh;
//import com.google.api.client.apache.ApacheHttpTransport;
import Exceptions.AuthenticationFailureException;
import Exceptions.BadParametersException;
import Repository.jdbc.Database;
import Util.auth.JWT;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.GeneralSecurityException;
import java.util.Collections;

public class GoogleLoginCommand implements Command {
    private String idTokenString;

    public GoogleLoginCommand(String accessTokenString) {
        this.idTokenString = accessTokenString;
    }

    @Override
    public String execute() throws AuthenticationFailureException, BadParametersException {
        if (idTokenString == null) {
            throw new BadParametersException();
        }
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("172.24.0.4", 3128));
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(
                new NetHttpTransport.Builder().setProxy(proxy).build(),
                new JacksonFactory())
                .setAudience(Collections.singletonList(Loghmeh.GOOGLE_AUTH_CLIENT_ID))
                .build();
        GoogleIdToken idToken;
        try {
            idToken = verifier.verify(idTokenString);
        } catch (GeneralSecurityException | IOException e) {
            throw new AuthenticationFailureException();
        }
        if (idToken != null) {
            Payload payload = idToken.getPayload();
            String email = payload.getEmail();
            System.out.println(email);
            Integer userId = Database.userRepository.getUserIdByEmail(email);
            if (userId == null) throw new AuthenticationFailureException();
            return JWT.generateJwtFor(userId);
        } else throw new AuthenticationFailureException();
    }
}
