package BusinessLogic;

import DTO.ProfileDTO;
import Data.Access.UserSessionAccess;
import Data.Manager.UserManager;
import Exceptions.UserNotFoundException;
import Model.User;

import java.util.Objects;

public class ViewUserProfileCommand implements Command {

    private Integer userId;

    public ViewUserProfileCommand(Integer userId) {
        this.userId = userId;
    }

    @Override
    public ProfileDTO execute() {
        User user = null;
        try {
            user = UserManager.getUserById(userId);
        } catch (UserNotFoundException e) {
            // never happens
        }
        return new ProfileDTO(Objects.requireNonNull(user));
    }
}
