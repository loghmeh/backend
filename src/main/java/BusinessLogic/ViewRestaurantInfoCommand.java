package BusinessLogic;

import Data.Access.DatabaseAccess;
import Data.Access.UserSessionAccess;
import Data.Manager.RestaurantManager;
import Data.Manager.UserManager;
import Model.Restaurant;
import Exceptions.NotEnoughParametersException;
import Exceptions.RestaurantDoesntExistException;
import Exceptions.RestaurantTooFarException;
import Model.User;

import static Util.Math.euclideanDistance;
import static Util.Math.getRestaurantDistanceToUser;

public class ViewRestaurantInfoCommand implements Command {
    private String restaurantId;
    private Integer userId;

    public ViewRestaurantInfoCommand(String restaurantId, Integer userId) {
        this.restaurantId = restaurantId;
        this.userId = userId;
    }

    @Override
    public Restaurant execute() throws NotEnoughParametersException, RestaurantTooFarException, RestaurantDoesntExistException {
        if (restaurantId == null) {
            throw new NotEnoughParametersException();
        }
        Restaurant restaurant = RestaurantManager.getRestaurantById(restaurantId);
        if (euclideanDistance(restaurant.getLocation(), UserManager.getUserLocation(userId)) > 170) {
            throw new RestaurantTooFarException();
        }
        return restaurant;
    }
}
