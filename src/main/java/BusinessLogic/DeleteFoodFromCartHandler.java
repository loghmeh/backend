package BusinessLogic;

import DTO.CartDTO;
import Data.Access.UserSessionAccess;
import Data.Manager.CartManager;
import Data.Manager.UserManager;
import Exceptions.*;
import Model.CartFoodItem;
import Model.User;

import java.util.ArrayList;

public class DeleteFoodFromCartHandler implements Command {

    private String restaurantId;
    private String foodId;
    private Integer userId;

    public DeleteFoodFromCartHandler(String restaurantId, String foodName, Integer userId) {
        this.restaurantId = restaurantId;
        this.foodId = foodName;
        this.userId = userId;
    }

    //TODO: This smells
    @Override
    public CartDTO execute()
            throws NotEnoughParametersException,
            FoodDoesntExistException,
            FoodDoesntExistInCartException {
        if (restaurantId == null || foodId == null) {
            throw new NotEnoughParametersException();
        }
        CartManager.deleteFoodFromCart(userId, restaurantId, foodId);
        CartDTO cart = new CartDTO(CartManager.findById(userId));
        return cart;
    }
}
