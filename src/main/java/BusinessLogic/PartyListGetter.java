package BusinessLogic;

import Data.Manager.LoghmehManager;
import Data.Manager.PartyFoodManager;
import Schedulers.FoodPartyInvoker;
import Services.FoodPartyList;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.TimerTask;

public class PartyListGetter extends TimerTask {
    public void run(){
        PartyFoodManager.inactivePartyFoods();
        long secondsToFoodPartyFinish = FoodPartyInvoker.getPeriodSec();
        LoghmehManager.setFoodPartyEndTime(
                LocalDateTime.now().plus(secondsToFoodPartyFinish, ChronoUnit.SECONDS)
        );
        FoodPartyList foodPartyList = new FoodPartyList();
        try {
            foodPartyList.sendGet();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
