package BusinessLogic;

import BusinessLogic.Util.CourierFinder;
import Data.Manager.CartManager;
import Data.Manager.UserManager;
import Exceptions.CartIsEmptyException;
import Exceptions.InsufficientBalanceToFinalizeOrderException;
import Exceptions.UserNotFoundException;
import Model.User;

import java.util.Objects;

public class FinalizeOrderCommand implements Command {

    private Integer userId;

    public FinalizeOrderCommand(Integer userId) {
        this.userId = userId;
    }

    @Override
    public Object execute() throws CartIsEmptyException, InsufficientBalanceToFinalizeOrderException {
        User currentUser = null;
        try {
            currentUser = UserManager.getUserById(userId);
        } catch (UserNotFoundException e) {
            // This doesn't happen. TODO: Refactor never-happening exceptions
        }
        int totalCartPrice = Objects.requireNonNull(currentUser).getCart().getTotalPrice();
        int userBalance = currentUser.getBalance();
        if(CartManager.isCartEmpty(currentUser)) {
            throw new CartIsEmptyException();
        }
        if (totalCartPrice > userBalance) {
            throw new InsufficientBalanceToFinalizeOrderException();
        }
        int orderId = CartManager.finalizeOrder(currentUser);
        CourierFinder.findCourierForOrder(orderId, currentUser.getId());
        return null;
    }
}
