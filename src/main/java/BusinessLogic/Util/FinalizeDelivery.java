package BusinessLogic.Util;

import Data.Manager.OrderManager;
import Model.Order;
import Model.OrderStatus;

import java.util.Timer;
import java.util.TimerTask;

public class FinalizeDelivery extends TimerTask {
    private int orderId;
    private Timer timer;

    public FinalizeDelivery(int orderId, Timer timer){
        this.orderId = orderId;
        this.timer = timer;
    }
    public void run(){
        OrderManager.changeOrderStatus(orderId, OrderStatus.DELIVERED);
        this.cancel();
        timer.cancel();
        timer.purge();
    }
}
