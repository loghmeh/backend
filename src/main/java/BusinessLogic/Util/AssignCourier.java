package BusinessLogic.Util;

import Data.Access.OrderAccess;
import Data.Manager.OrderManager;
import Data.Manager.UserManager;
import Exceptions.UserNotFoundException;
import Model.Courier;
import Model.Location;
import Model.Restaurant;
import Services.ExternalAPIs;
import Util.Time;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class AssignCourier extends TimerTask {
    private int orderId;
    private int userId;
    private Timer timer;

    AssignCourier(int orderId, int userId, Timer timer) {
        this.orderId = orderId;
        this.userId = userId;
        this.timer = timer;
    }
//TODO: Remove Exceptions from Access Layer?!
    public void run() {
        List<Courier> couriers = ExternalAPIs.getAvailableCouriers();
        if (!couriers.isEmpty()) {
            Restaurant restaurant = OrderAccess.getOrderRestaurant(orderId);
            double minEta = Double.MAX_VALUE;
            Location restLoc = restaurant.getLocation();
            Location userLoc = null;
            try {
                userLoc = UserManager.getUserById(userId).getLocation();
            } catch (UserNotFoundException e) {
                // stfu you never-happening exception
            }
            for (Courier courier: couriers) {
                Location courLoc = courier.getLocation();
                double eta = Time.calculateEta(restLoc, courLoc, Objects.requireNonNull(userLoc), courier.getVelocity());
                if (eta < minEta) {
                    minEta = eta;
                }
            }
            LocalDateTime eta = LocalDateTime.now().plus((long)minEta, ChronoUnit.SECONDS);
            OrderManager.setInDelivery(orderId, eta);
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.SECOND, (int)minEta);
            Timer deliveryTimer = new Timer();
            deliveryTimer.schedule(new FinalizeDelivery(orderId, deliveryTimer), calendar.getTime());
            this.cancel();
            timer.cancel();
            timer.purge();
        }
    }
}
