package BusinessLogic.Util;

import java.util.Timer;

public class CourierFinder {
    public static void findCourierForOrder(int orderId, int userId){
        Timer timer = new Timer();
        timer.schedule(new AssignCourier(orderId, userId, timer), 0, 5000);
    }
}
