package BusinessLogic;

import Data.Manager.FoodManager;
import Data.Manager.RestaurantManager;
import Model.Food;
import Model.Restaurant;
import Model.User;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class GetRestaurantSearchResultsCommand implements Command {
    private String foodNameSearch;
    private String restaurantNameSearch;

    public GetRestaurantSearchResultsCommand(String foodNameSearch, String restaurantNameSearch){
        this.foodNameSearch = foodNameSearch == null ? "" : foodNameSearch;
        this.restaurantNameSearch = restaurantNameSearch == null ? "" : restaurantNameSearch;
    }

    @Override
    public ArrayList<Restaurant> execute() throws Exception {
        ArrayList<Restaurant> availableRestaurants = new ViewAvailableRestaurantsCommand().execute();

        ArrayList<Restaurant> foundRestaurants;
        ArrayList<Restaurant> foundFoods;
        if(!restaurantNameSearch.equalsIgnoreCase("")) {
            foundRestaurants = RestaurantManager.searchByName(restaurantNameSearch);
        }
        else {
            foundRestaurants = new ArrayList<>();
        }

        if(!foodNameSearch.equalsIgnoreCase("")) {
            foundFoods = FoodManager.searchRestaurantsByFoodName(foodNameSearch);
        }
        else {
            foundFoods = new ArrayList<>();
        }

        Set<Restaurant> set = new LinkedHashSet<>(foundRestaurants);
        set.addAll(foundFoods);
        ArrayList<Restaurant> searchResult = new ArrayList<>(set);

//        ArrayList<Restaurant> availableFoundByRestaurant = new ArrayList<Restaurant>(availableRestaurants);
//        availableFoundByRestaurant.retainAll(foundRestaurants);

//        ArrayList<Restaurant> availableFoundByFood = new ArrayList<>();
//        for(Food food: foundFoods)
//            for(Restaurant restaurant: availableRestaurants)
//                if(!Util.FoodUtil.isPartyFood(food.getId()))
//                    for(Food menuItem: restaurant.getMenu())
//                        if(menuItem.getId().equalsIgnoreCase(food.getId()))
//                            availableFoundByFood.add(restaurant);
//                else if(restaurant.getPartyMenu().contains(food))
//                    for(Food partyMenuItem: restaurant.getPartyMenu())
//                        if(partyMenuItem.getId().equalsIgnoreCase(food.getId()))
//                            availableFoundByFood.add(restaurant);

//         ArrayList<Restaurant> searchResult = new ArrayList<Restaurant>(availableFoundByRestaurant);
//         searchResult.addAll(availableFoundByFood);
         return searchResult;

    }
}
