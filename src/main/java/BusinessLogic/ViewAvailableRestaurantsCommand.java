package BusinessLogic;

import Data.Access.DatabaseAccess;
import Data.Access.UserSessionAccess;
import Data.Manager.RestaurantManager;
import Exceptions.BadParametersException;
import Model.Restaurant;
import Exceptions.NoNearbyRestaurantAvailableException;
import Model.User;

import java.util.ArrayList;

import static Util.Math.getRestaurantDistanceToUser;

public class ViewAvailableRestaurantsCommand implements Command{

    private String pageId;
    private String pageChunk;
    private boolean wantAll;

    public ViewAvailableRestaurantsCommand(String pageId, String pageChunk){
        this.pageId = pageId;
        this.pageChunk = pageChunk;
        this.wantAll = false;
    }

    public ViewAvailableRestaurantsCommand(){
        this.wantAll = true;
    }

    @Override
    public ArrayList<Restaurant> execute() throws NoNearbyRestaurantAvailableException, BadParametersException {
//        ArrayList<Restaurant> availableRestaurants = new ArrayList<>();
//        for (Restaurant restaurant: RestaurantManager.getRestaurants()) {
//            if(Util.Math.euclideanDistance(restaurant.getLocation(), currentUser.getLocation()) <= 170) {
//                availableRestaurants.add(restaurant);
//            }
//        }
//        if (availableRestaurants.size() == 0) {
//            throw new NoNearbyRestaurantAvailableException();
//        }
//        return availableRestaurants;
//    }
        ArrayList<Restaurant> restaurants = new ArrayList<>();
        if (this.wantAll) {
            restaurants = RestaurantManager.getAllRestaurants();
        }
        else {
            try {
                int pageId = Integer.parseInt(this.pageId);
                int pageChunk = Integer.parseInt(this.pageChunk);
                restaurants = RestaurantManager.getRestaurants(pageId, pageChunk);
            }
            catch (NumberFormatException e) {
                throw new BadParametersException();
            }
        }
        if (restaurants.size() == 0) {
            throw new NoNearbyRestaurantAvailableException();
        }
        return restaurants;
    }
}
