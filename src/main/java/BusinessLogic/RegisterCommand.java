package BusinessLogic;

import Data.Manager.UserManager;
import Exceptions.BadParametersException;
import Exceptions.UserWithEmailExistsException;
import Model.Location;
import Model.User;
import Util.Hash.Hashing;
import Util.RangedRandomGenerator;

public class RegisterCommand implements Command{
    private String email;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String password;

    public RegisterCommand(String email, String fn, String ln, String pw, String pn){
        this.email = email;
        this.firstName = fn;
        this.lastName = ln;
        this.password = pw;
        this.phoneNumber = pn;
    }

    public Object execute() throws UserWithEmailExistsException, BadParametersException {
        if(email == null || firstName == null ||
                lastName == null || password == null ||
                phoneNumber == null) throw new BadParametersException();
        double x = RangedRandomGenerator.getRandomDoubleBetweenRange(-180, 180);
        double y = RangedRandomGenerator.getRandomDoubleBetweenRange(-180, 180);
        Location location = new Location(x, y);
        UserManager.addUser(firstName, lastName, password, email, phoneNumber, location);
        return null;
    }
}
