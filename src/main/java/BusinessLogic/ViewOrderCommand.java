package BusinessLogic;

import Data.Access.UserSessionAccess;
import Data.Manager.OrderManager;
import Exceptions.NotEnoughParametersException;
import Exceptions.OrderNotFoundException;
import Model.Order;
import Model.User;

public class ViewOrderCommand implements Command {
    private Integer orderId;

    public ViewOrderCommand(Integer orderId) {
        this.orderId = orderId;
    }

    @Override
    public Order execute() throws NotEnoughParametersException, OrderNotFoundException {
        if (orderId == null) {
            throw new NotEnoughParametersException();
        }
        //TODO: Check if order is for another User and trhow Exception (Use combination of UserID and OrderID?)
        Order order = OrderManager.findById(orderId);
        if (order == null) {
            throw new OrderNotFoundException();
        }
        return order;
    }
}
