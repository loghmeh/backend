package BusinessLogic;

import Commands.Response.StatusResponse;
import Data.Access.UserSessionAccess;
import Data.Manager.UserManager;
import Exceptions.NegativeOrZeroChargeAmountException;
import Exceptions.NotEnoughParametersException;
import Model.User;

public class ChargeAccountCommand implements Command {
    private Integer amount;
    private Integer userId;

    public ChargeAccountCommand(Integer amount, Integer userId) {
        this.amount = amount;
        this.userId = userId;
    }

    @Override
    public Object execute() throws NegativeOrZeroChargeAmountException, NotEnoughParametersException {
        if (amount == null) {
            throw new NotEnoughParametersException();
        }
        if(amount <= 0){
            throw new NegativeOrZeroChargeAmountException();
        }
        UserManager.chargeAccount(userId, amount);
        return null;
    }
}
