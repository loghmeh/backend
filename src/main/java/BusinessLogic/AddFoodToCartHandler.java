package BusinessLogic;

import DTO.CartDTO;
import Data.Manager.CartManager;
import Data.Manager.PartyFoodManager;
import Exceptions.*;
import Model.*;

public class AddFoodToCartHandler implements Command {

    private String restaurantId;
    private String foodId;
    private Integer userId;

    public AddFoodToCartHandler(String restaurantId, String foodName, Integer userId) {
        this.restaurantId = restaurantId;
        this.foodId = foodName;
        this.userId = userId;
    }

    @Override
    public CartDTO execute() throws NotEnoughParametersException, CartCanOnlyContainFoodsFromOneRestaurantException, FoodDoesntExistException, CartCanOnlyContainFoodsFromOneCategoryException, PartyFoodFinishedException, FoodPartyEndedException, PartyFoodDoesNotExistsException {
        if (restaurantId == null || foodId == null) {
            throw new NotEnoughParametersException();
        }
        Cart userCart = CartManager.findById(userId);
        if (Util.FoodUtil.isPartyFood(foodId)) {
            PartyFood food = PartyFoodManager.getPartyFood(foodId);
            CartFoodItem cartFoodItem = userCart.getFoods().stream()
                    .filter(foodItem -> foodId.equals(foodItem.getId()))
                    .findAny()
                    .orElse(null);

            if (cartFoodItem != null && !PartyFoodManager.sufficientCount(cartFoodItem.getCount(), cartFoodItem.getId())) {
                throw new PartyFoodFinishedException();
            }
//            if (food.getCount() <= 0)
//                throw new PartyFoodFinishedException();
            if (food.getStatus().equals(PartyStatus.INACTIVE))
                throw new FoodPartyEndedException();
        }
        CartManager.addFoodToCart(userCart, userId, restaurantId, foodId); // Can't it return current Cart?
        CartDTO cart = new CartDTO(CartManager.findById(userId));
        return cart;
    }
}
