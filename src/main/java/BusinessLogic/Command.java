package BusinessLogic;

import Model.User;

public interface Command {
    Object execute() throws Exception;
}
