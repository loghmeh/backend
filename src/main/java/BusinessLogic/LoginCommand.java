package BusinessLogic;

import Data.Manager.UserManager;
import Exceptions.AuthenticationFailureException;
import Exceptions.NotEnoughParametersException;
import Exceptions.UserNotFoundException;
import Repository.jdbc.Database;

public class LoginCommand implements Command {
    private String email;
    private String password;

    public LoginCommand(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @Override
    public String execute() throws NotEnoughParametersException, AuthenticationFailureException {
        if (email == null || password == null) throw new NotEnoughParametersException();
        String hashedPassword = password; //TODO: Hash!
        Integer userId = Database.userRepository.getUserIdByEmailAndPassword(email, hashedPassword);
        if (userId == null) throw new AuthenticationFailureException();
        return Util.auth.JWT.generateJwtFor(userId);
    }
}
