package Data.Access;

import Model.Database;
import Model.Food;
import Model.PartyFood;
import Model.Restaurant;
import Exceptions.FoodAlreadyExistsException;
import Exceptions.FoodDoesntExistException;
import Exceptions.RestaurantAlreadyExistsException;
import Exceptions.RestaurantDoesntExistException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class DatabaseAccess {
    private static Database db = Database.getInstance();

    public static void addRestaurants(List<Restaurant> restList) throws RestaurantAlreadyExistsException {
        for(Restaurant rest: restList){
            addRestaurant(rest);
        }
    }
    public static void addRestaurant(Restaurant newRestaurant) throws RestaurantAlreadyExistsException {
        if (db.getRestaurants().containsKey(newRestaurant.getId())) {
            throw new RestaurantAlreadyExistsException();
        }
        db.addRestaurant(newRestaurant);
    }

    public static boolean foodExistsInRestaurant(String restaurantId, String foodId) {
        Restaurant restaurant = db.getRestaurants().get(restaurantId);
        if(Util.FoodUtil.isPartyFood(foodId)) {
            for(PartyFood partyFood: restaurant.getPartyMenu()) {
                if(partyFood.getId().equals(foodId)) {
                    return true;
                }
            }
        }
        else {
            for(Food food: restaurant.getMenu()) {
                if(food.getId().equals(foodId)) {
                    return true;
                }
            }
        }
        return false;

    }

    public static void addFood(Food newFood, String restaurantId) throws FoodAlreadyExistsException, RestaurantDoesntExistException {
        if (!db.getRestaurants().containsKey(restaurantId)) {
            throw new RestaurantDoesntExistException();
        }
        if (foodExistsInRestaurant(restaurantId, newFood.getId())) {
            throw new FoodAlreadyExistsException();
        }
        db.getRestaurants().get(restaurantId).addFood(newFood);
    }

    public static ArrayList<String> getRestaurantNames() {
        return new ArrayList<>(db.getRestaurants().keySet());
    }

    public static ArrayList<Restaurant> getRestaurants() {
        return new ArrayList<>(db.getRestaurants().values());
    }

    public static Restaurant getRestaurantById(String id) {
        if(!db.getRestaurants().containsKey(id)) {
            return null;
        }
        return db.getRestaurants().get(id);
    }

    public static PartyFood getPartyFood(String restaurantId, String foodId) throws RestaurantDoesntExistException, FoodDoesntExistException {
        if (!db.getRestaurants().containsKey(restaurantId)) {
            throw new RestaurantDoesntExistException();
        }
        Restaurant restaurant = db.getRestaurants().get(restaurantId);
        if (!foodExistsInRestaurant(restaurantId, foodId)) {
            throw new FoodDoesntExistException();
        }
        return restaurant.getPartyMenu().stream()
                .filter(partyFood -> foodId.equals(partyFood.getId()))
                .findAny()
                .orElse(null);
    }

    public static Food getFood(String restaurantId, String foodId) throws RestaurantDoesntExistException, FoodDoesntExistException {
        if (!db.getRestaurants().containsKey(restaurantId)) {
            throw new RestaurantDoesntExistException();
        }
        Restaurant restaurant = db.getRestaurants().get(restaurantId);
        if (!foodExistsInRestaurant(restaurantId, foodId)) {
            throw new FoodDoesntExistException();
        }
        return restaurant.getMenu().stream()
                .filter(food -> foodId.equals(food.getId()))
                .findAny()
                .orElse(null);
    }

    public static void setFoodPartyEndTime(LocalDateTime endTime) {
        db.setCurrentFoodPartyFinishTime(endTime);
    }
    //TODO: Add above and below methods to Manager classes
    public static LocalDateTime getFoodPartyEndTime() {
        return db.getCurrentFoodPartyFinishTime();
    }

}
