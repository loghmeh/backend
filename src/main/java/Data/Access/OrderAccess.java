package Data.Access;

import Model.Order;
import Model.Restaurant;
import Repository.jdbc.Database;

public class OrderAccess {

    public static Restaurant getOrderRestaurant(int orderId) {
        Order order = Database.orderRepository.findById(orderId);
        return Database.restaurantRepository.findById(order.getRestaurantId());
    }
}
