package Data.Access;

import Exceptions.*;
import Model.*;

import java.util.ArrayList;

public class UserSessionAccess {
//    private static User user = User.getInstance();
    private static User user = new User();

    public static Location getUserLocation() {
        return user.getLocation();
    }

    public static ArrayList<CartFoodItem> getUserCartFoodItems() {
        return user.getCart().getFoods();
    }

    public static Cart getUserCart() {
        return user.getCart();
    }

    public static void addFoodToCart(String restaurantId, String foodId) throws FoodDoesntExistException, RestaurantDoesntExistException, CartCanOnlyContainFoodsFromOneRestaurantException, CartCanOnlyContainFoodsFromOneCategoryException {
        user.getCart().addFood(restaurantId, foodId);
    }

    public static Restaurant getCartRestaurant() {
        return DatabaseAccess.getRestaurantById(user.getCart().getRestaurantId());
    }

    public static Order finalizeUserOrder() {
        return user.finalizeOrder();
    }

    public static void chargeAccount(Integer amount) {
        user.setBalance(user.getBalance() + amount);
    }

    public static void depositAccount(Integer amount) throws NegativeUserBalanceException {
        if (user.getBalance() - amount < 0) {
            throw new NegativeUserBalanceException();
        }
        user.setBalance(user.getBalance() - amount);
    }

    public static int getTotalCartPrice() {
        return user.getCart().getTotalPrice();
    }

    public static int getUserBalance() {
        return user.getBalance();
    }

    public static User getUserInfo() {
        return user; //TODO: is this good?
    }

    public static Order getOrderById(int orderId) {
        for (Order order: user.getOrders()) {
            if(order.getId() == orderId) {
                return order;
            }
        }
        return null;
    }

    public static void addPartyFoodToCart(String restaurantId, String foodId) throws RestaurantDoesntExistException, CartCanOnlyContainFoodsFromOneCategoryException, CartCanOnlyContainFoodsFromOneRestaurantException, FoodDoesntExistException {
        user.getCart().addPartyFood(restaurantId, foodId);

    }

    public static void deleteFoodFromCart(String restaurantId, String foodId) throws RestaurantDoesntExistException, FoodDoesntExistException, FoodDoesntExistInCartException {
        user.getCart().deleteFood(restaurantId, foodId);
    }
}
