package Data.Manager;

import Exceptions.*;
import Model.PartyFood;
import Repository.jdbc.Database;

import java.util.ArrayList;

public class PartyFoodManager {

    public static PartyFood getPartyFood(String foodId) throws PartyFoodDoesNotExistsException {
        //TODO: return null if not found and check null instead of another DB call
        if(Database.partyFoodRepository.existsById(foodId))
            return Database.partyFoodRepository.findById(foodId);
        throw new PartyFoodDoesNotExistsException();
    }

    public static void addPartyFood(PartyFood partyFood) throws FoodAlreadyExistsException {
        Database.partyFoodRepository.add(partyFood);
    }

    public static void addPartyFoods(ArrayList<PartyFood> partyFoods) throws PartyFoodExistsException {
        for(PartyFood partyFood: partyFoods){
            if(Database.partyFoodRepository.existsById(partyFood.getId()))
                throw new PartyFoodExistsException();
        }
        Database.partyFoodRepository.addBulk(partyFoods);
    }

    public static void updatePartyFoodCount(String foodId, int ordered) {
        PartyFood partyFood = Database.partyFoodRepository.findById(foodId);
        int count = partyFood.getCount();
        count = count - ordered;
        partyFood.setCount(count);
        Database.partyFoodRepository.updateCount(partyFood);
    }

    public static void inactivePartyFoods() {
        Database.partyFoodRepository.updateAllStatus();
    }

    public static boolean isPartyFoodCountSufficient(PartyFood partyFood, int ordering){
        int count = partyFood.getCount();
        if(count < ordering)
            return false;
        return true;
    }

    public static boolean sufficientCount(int count, String foodId){
        return Database.partyFoodRepository.sufficientCount(count, foodId);
    }
    public static void foodPartyFinished(){
        Database.partyFoodRepository.updateAllStatus();
    }
}
