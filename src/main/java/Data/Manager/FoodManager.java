package Data.Manager;

import Exceptions.FoodAlreadyExistsException;
import Exceptions.FoodDoesntExistException;
import Exceptions.FoodExistsInDatabaseException;
import Exceptions.RestaurantDoesntExistException;
import Model.Food;
import Model.Restaurant;
import Repository.jdbc.Database;

import java.util.ArrayList;

public class FoodManager {
    public static void addFood(Food food) throws FoodAlreadyExistsException {
        if(Database.foodRepository.existsById(food.getId()))
            throw new FoodAlreadyExistsException();
        Database.foodRepository.add(food);

    }

    public static void addFoods(ArrayList<Food> foods) throws FoodAlreadyExistsException {
        for(Food food: foods){
            if(Database.foodRepository.existsById(food.getId()))
                throw new FoodAlreadyExistsException();
        }
        Database.foodRepository.addBulk(foods);
    }

    public static Food getFood(String restaurantId, String foodId) throws FoodDoesntExistException {
        if(Database.foodRepository.existsById(foodId))
            return Database.foodRepository.findById(foodId);
        throw new FoodDoesntExistException();
    }

    public static ArrayList<Restaurant> searchRestaurantsByFoodName(String searchName){
        ArrayList<String> restaurantIds = Database.foodRepository.searchRestaurantIdsByFoodName(searchName);
        ArrayList<Restaurant> restaurants = new ArrayList<>();
        for(String restaurantId: restaurantIds) {
            Restaurant restaurant = Database.restaurantRepository.findById(restaurantId);
            restaurants.add(restaurant);
        }
        return restaurants;
    }

}
