package Data.Manager;

import Exceptions.UserNotFoundException;
import Exceptions.UserWithEmailExistsException;
import Model.Cart;
import Model.Order;
import Repository.jdbc.Database;
import Model.Location;
import Model.User;

import java.util.ArrayList;

import static Repository.jdbc.Database.orderRepository;

public class UserManager {
    public static User addUser(String firsname, String lastname, String password, String email, String phoneNumber, Location location) throws UserWithEmailExistsException {
        if(Database.userRepository.userExists(email))
            throw new UserWithEmailExistsException();
        User newUser = new User();
        newUser.setFirstName(firsname);
        newUser.setLastName(lastname);
        newUser.setEmail(email);
        newUser.setPhoneNumber(phoneNumber);
        newUser.setLocation(location);
        newUser.setBalance(0);
        Integer userId = Database.userRepository.addUser(newUser, password);
        if (userId == null) {
            System.out.println("USERID IS NULL!");
            return null;
        }
        Cart newCart = Database.cartRepository.createCart(userId);
        if (newCart == null) {
            System.out.println("CART IS NULL!");
            return null;
        }
        newUser.setId(userId);
        newUser.setCart(newCart);
        newUser.setOrders(new ArrayList<>());
        return newUser;
    }

    public static Location getUserLocation(int userId) {
        User user = Database.userRepository.findById(userId); //TODO: We need a direct getLocation method
        return user.getLocation();
    }

    public static User getUserById(int userId) throws UserNotFoundException {
        User user = Database.userRepository.findById(userId);
        if (user == null) {
            throw new UserNotFoundException();
        }
        Cart userCart = Database.cartRepository.findById(userId);
        user.setCart(userCart);
        ArrayList<Order> userOrders = Database.orderRepository.findByUserId(userId);
        user.setOrders(userOrders);
        return user;
    }


    public static void chargeAccount(int userId, int amount) {
        //TODO: we must get userId as argument or is this ok?
        // because we only charge current user's account balance
//        user.setBalance(user.getBalance() + amount);
        Database.userRepository.increaseBalance(userId, amount);
    }
}
