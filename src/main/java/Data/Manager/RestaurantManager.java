package Data.Manager;

import Exceptions.RestaurantAlreadyExistsException;
import Exceptions.RestaurantDoesntExistException;
import Model.Food;
import Model.PartyFood;
import Model.Restaurant;
import Repository.jdbc.Database;

import java.util.ArrayList;

public class RestaurantManager {
    public static void addRestaurants(ArrayList<Restaurant> restList) throws RestaurantAlreadyExistsException {
        for(Restaurant rest: restList){
            addRestaurant(rest);
        }
    }

    public static void addRestaurant(Restaurant newRestaurant) throws RestaurantAlreadyExistsException {
        if (Database.restaurantRepository.restaurantExists(newRestaurant.getId())) {
            throw new RestaurantAlreadyExistsException();
        }
//        for(Food food: newRestaurant.getMenu()) {
//            food.setRestaurantId(newRestaurant.getId());
//        }
//        for(PartyFood food: newRestaurant.getPartyMenu()) {
//            food.setRestaurantId(newRestaurant.getId());
//        }
        Database.restaurantRepository.addRestaurant(newRestaurant);
        Database.foodRepository.addBulk(newRestaurant.getMenu());
        Database.partyFoodRepository.addBulk(newRestaurant.getPartyMenu());
    }

    public static ArrayList<Restaurant> getAllRestaurants() {
        return Database.restaurantRepository.findAll();
    }

    public static ArrayList<Restaurant> getRestaurants(int pageId, int pageChunk) {
        return Database.restaurantRepository.find(pageId, pageChunk);
    }

    public static Restaurant getRestaurantById(String restaurantId) throws RestaurantDoesntExistException {
        Restaurant restaurant = Database.restaurantRepository.findById(restaurantId);
        if (restaurant == null) {
            throw new RestaurantDoesntExistException();
        }
        return restaurant;
    }

    public static ArrayList<Restaurant> searchByName(String searchName){
        return Database.restaurantRepository.searchByName(searchName);
    }
}
