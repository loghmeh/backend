package Data.Manager;

import Model.Location;
import Model.Order;
import Model.OrderStatus;
import Model.User;
import Repository.jdbc.Database;
import Util.Time;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class OrderManager {
    public static Integer addOrder(Location restaurantLocation, String restaurantId, Location userLocation, int userId){
        Order order = new Order(restaurantId);
        order.setEta(Time.estimateEta(userLocation, restaurantLocation));
        // TODO: Assign courier and set order in DELIVERTING
        return Database.orderRepository.add(order, userId);
    }

    public static ArrayList<Order> findUserOrders(int userId){
        ArrayList<Order> orders = Database.orderRepository.findByUserId(userId);
        return orders;
    }

    public static void changeOrderStatus(int orderId, OrderStatus orderStatus){
        Database.orderRepository.updateStatus(orderId, orderStatus.getValue());
    }

    public static void setInDelivery(int orderId, LocalDateTime eta) {
        Database.orderRepository.updateStatusAndEta(
                orderId,
                OrderStatus.DELIVERING.getValue(),
                Util.Time.dateToString(eta)
        );
    }

    public static Order findById(int orderId){
        return Database.orderRepository.findById(orderId);
    }
}
