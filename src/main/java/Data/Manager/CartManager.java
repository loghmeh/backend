package Data.Manager;

import Exceptions.*;
import Model.*;
import Repository.jdbc.Database;
import Util.FoodUtil;

public class CartManager {

    public static void addFoodToCart(Cart userCart, int userId, String restaurantId, String foodId) throws CartCanOnlyContainFoodsFromOneRestaurantException, CartCanOnlyContainFoodsFromOneCategoryException, FoodDoesntExistException {
        CartMode validCartModeForThisTranscation = Util.FoodUtil.isPartyFood(foodId)
                ? CartMode.FOODPARTY
                : CartMode.NORMAL;

        if (userCart.getRestaurantId() != null && !userCart.getRestaurantId().equals(restaurantId)) {
            throw new CartCanOnlyContainFoodsFromOneRestaurantException();
        }
        if(userCart.getCartMode() != CartMode.EMPTY && !userCart.getCartMode().equals(validCartModeForThisTranscation)) {
            throw new CartCanOnlyContainFoodsFromOneCategoryException();
        }
        Food newFood = FoodManager.getFood(restaurantId, foodId);
        if (userCart.getCartMode().equals(CartMode.EMPTY)) {
            userCart.setRestaurantId(restaurantId);
            userCart.setCartMode(validCartModeForThisTranscation);

            Database.cartRepository.updateCart(
                    userId,
                    userCart.getRestaurantId(),
                    userCart.getCartMode()
            );
        }
        CartFoodItemManager.addCartFoodItem(new CartFoodItem(newFood, 1), userId);
    }

    public static Cart findById(int id) {
        return Database.cartRepository.findById(id);
    }

    public static int finalizeOrder(User currentUser) {
        // Steps:
        // 1. If cart mode is FOODPARTY, ecrease available counts
        // 2. Deposit user balance
        // 3. Add new Order
        // 4. Clear Cart
        // 5. Change CartFoodItems to remove Cart ID and add Order ID
        Cart currentCart = currentUser.getCart();
        if (currentCart.getCartMode().equals(CartMode.FOODPARTY)) {
            for(CartFoodItem foodItem : currentCart.getFoods()) {
                PartyFoodManager.updatePartyFoodCount(foodItem.getId(), foodItem.getCount());
            }
        }
        int totalPrice = currentCart.getTotalPrice();
//        currentUser.setBalance(currentUser.getBalance() - totalPrice);
        Database.userRepository.increaseBalance(currentUser.getId(), -totalPrice);
        Restaurant restaurant = Database.restaurantRepository.findById(currentCart.getRestaurantId());
        int orderId = OrderManager.addOrder(
                restaurant.getLocation(), restaurant.getId(),
                currentUser.getLocation(), currentUser.getId()
        );
        Database.cartRepository.updateCart(currentUser.getId(), null, CartMode.EMPTY); //TODO: Shall we return Cart now?!
        CartFoodItemManager.changeFromCartToOrder(currentUser.getId(), orderId);
        return orderId;
    }

    public static boolean isCartEmpty(User currentUser) {
        return CartFoodItemManager.isCartEmpty(currentUser.getId());
    }

    public static void deleteFoodFromCart(int userId, String restaurantId, String foodId) throws FoodDoesntExistException, FoodDoesntExistInCartException {
        Food deletedFood = FoodUtil.isPartyFood(foodId)
                ? Database.partyFoodRepository.findById(foodId)
                : Database.foodRepository.findById(foodId);
        if (deletedFood == null) {
            throw new FoodDoesntExistException();
        }
        boolean foodExistsInCart = false;
        Cart userCart = findById(userId);
        for (CartFoodItem foodItem : userCart.getFoods()) {
            if (foodItem.equals(deletedFood)) {
                CartFoodItemManager.deleteCartFoodItem(foodItem, userId);
                foodExistsInCart = true;
                break;
            }
        }
        if (!foodExistsInCart) {
            throw new FoodDoesntExistInCartException();
        }

        if(CartFoodItemManager.isCartEmpty(userId)) {
            Database.cartRepository.updateCart(userId, null, CartMode.EMPTY);
        }
    }

}
