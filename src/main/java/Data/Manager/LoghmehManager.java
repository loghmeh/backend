package Data.Manager;

import Config.Loghmeh;
import redis.clients.jedis.Jedis;

import java.time.LocalDateTime;

public class LoghmehManager {
    private static final String FOODPARTY_FINISH_TIME_KEY = "FOODPARTY_FINISH";
    private static final String redisHost = Loghmeh.REDIS_HOST;
    private static final int redisPort = Integer.parseInt(Loghmeh.REDIS_PORT);

    public static LocalDateTime getFoodPartyEndTime() {
        Jedis jedis = new Jedis(redisHost, redisPort);
        return Util.Time.stringToDate(jedis.get(LoghmehManager.FOODPARTY_FINISH_TIME_KEY));
    }

    public static void setFoodPartyEndTime(LocalDateTime finishTime) {
        Jedis jedis = new Jedis(redisHost, redisPort);
        jedis.set(LoghmehManager.FOODPARTY_FINISH_TIME_KEY, Util.Time.dateToString(finishTime));
    }
}
