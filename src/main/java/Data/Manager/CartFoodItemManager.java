package Data.Manager;

import Model.Cart;
import Model.CartFoodItem;
import Repository.jdbc.Database;

import java.util.ArrayList;

public class CartFoodItemManager {

    public static void addCartFoodItem(CartFoodItem cartFoodItem, int cartId){
        if(Database.cartFoodItemRepository.existsByCartId(cartFoodItem, cartId)) {
             int count = Database.cartFoodItemRepository.findByCartIdAndId(cartId, cartFoodItem.getId()).getCount();
             count = cartFoodItem.getCount() + count;
             cartFoodItem.setCount(count);
             Database.cartFoodItemRepository.updateByCount(cartFoodItem, cartId);
        }

        else {
            Database.cartFoodItemRepository.add(cartFoodItem, cartId);
        }
    }

    public static void deleteCartFoodItem(CartFoodItem cartFoodItem, int cartId){
        if (cartFoodItem.getCount() > 1) {
            int count = cartFoodItem.getCount() - 1;
            cartFoodItem.setCount(count);
            Database.cartFoodItemRepository.updateByCount(cartFoodItem, cartId);
        }
        else {
            Database.cartFoodItemRepository.deleteFromCart(cartFoodItem.getId(), cartId);
        }
    }

    public static boolean cartFoodItemExistsInCart(int cartId, String foodId){
        ArrayList<CartFoodItem> cartFoodItems = Database.cartFoodItemRepository.findByCartId(cartId);
        for(CartFoodItem cartFoodItem: cartFoodItems) {
            if (cartFoodItem.getId().equals(foodId))
                return true;
        }
        return false;
    }
    public static void changeFromCartToOrder(int cartId, int orderId){
        Database.cartFoodItemRepository.updateByOrderId(cartId, orderId);
    }

    public static boolean isCartEmpty(int cartId){
        return Database.cartFoodItemRepository.isCartEmpty(cartId);
    }

    public static ArrayList<CartFoodItem> findByCartId(int cartId){
        return Database.cartFoodItemRepository.findByCartId(cartId);
    }
}
