package DatabaseTests.ManagerTests;

import Data.Manager.RestaurantManager;
import Exceptions.RestaurantAlreadyExistsException;
import Model.Location;
import Model.Restaurant;
import Repository.jdbc.Database;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import static Repository.jdbc.Database.restaurantRepository;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RestaurantManagerTests {
    @Before
    public void setUp() {
//        Database.truncateAll();
        //TODO: The above doesn't work (at least, easily). Find a way to run tests in db?
    }

    @Test
    public void addAndGetAllTest() {
        Restaurant r1, r2;
        try {
            r1 = new Restaurant(
                    "5",
                    "اکبر جوجه",
                    new Location(1, 1),
                    new URL("https://google.com")
            );
            r2 = new Restaurant(
                    "6",
                    "اصغر جوجه",
                    new Location(1, 2),
                    new URL("https://yahoo.com")
            );
            RestaurantManager.addRestaurant(r1);
            RestaurantManager.addRestaurant(r2);
        } catch (RestaurantAlreadyExistsException | MalformedURLException e) {
            r1 = null;
            r2 = null;
            System.out.println("Rid");
        }

        ArrayList<Restaurant> restaurants = RestaurantManager.getAllRestaurants();
        assertEquals(2, restaurants.size());
        assertThat(restaurants, hasItems(r1, r2));
    }

    @Test
    public void searchTest(){
        Restaurant r1, r2;
        try {
            r1 = new Restaurant(
                    "1",
                    "جوجه اصغر",
                    new Location(1, 1),
                    new URL("https://google.com")
            );
            r2 = new Restaurant(
                    "2",
                    "جوجه اکبر",
                    new Location(1, 2),
                    new URL("https://yahoo.com")
            );
            RestaurantManager.addRestaurant(r1);
            RestaurantManager.addRestaurant(r2);
        } catch (RestaurantAlreadyExistsException | MalformedURLException e) {
            r1 = null;
            r2 = null;
            System.out.println("Rid");
        }

        ArrayList<Restaurant> foundRestaurants = RestaurantManager.searchByName("جوجه");
        assertThat(foundRestaurants, hasItems(r1, r2));
    }

    @Test
    public void searchTestWithComplicatedNames(){
        Restaurant r1, r2;
        try {
            r1 = new Restaurant(
                    "7",
                    "اکبر جوجه",
                    new Location(1, 1),
                    new URL("https://google.com")
            );
            r2 = new Restaurant(
                    "8",
                    "اصغر جوجه علی آقا",
                    new Location(1, 2),
                    new URL("https://yahoo.com")
            );
            RestaurantManager.addRestaurant(r1);
            RestaurantManager.addRestaurant(r2);
        } catch (RestaurantAlreadyExistsException | MalformedURLException e) {
            r1 = null;
            r2 = null;
            System.out.println("Rid");
        }

        ArrayList<Restaurant> foundRestaurants = RestaurantManager.searchByName("وجه");
        assertThat(foundRestaurants, hasItems(r1, r2));
    }
}
