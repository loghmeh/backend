package DatabaseTests.RepositoryTests;

import Data.Manager.CartManager;
import Data.Manager.RestaurantManager;
import Exceptions.RestaurantAlreadyExistsException;
import Model.*;
import Repository.jdbc.Database;
import Repository.jdbc.User.UserRepository;
import Util.Time;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class OrderRepositoryTests {

    @Test
    public void findAndAddOrderByUserIdTest() throws MalformedURLException {
                User user = new User();
                user.setLocation(new Location(1, 2));
                user.setFirstName("Mammad");
                user.setLastName("Naseri");
                user.setEmail("mammad@naseri.loghmeh");
                user.setPhoneNumber("09396962772");
                user.setBalance(10000);
                int userId = Database.userRepository.addUser(user, "123456");
                Cart cart = Database.cartRepository.createCart(userId);
                Food keb = Database.foodRepository.findById("1");
                Food jooj = Database.foodRepository.findById("2");
                CartFoodItem cartFood1 = new CartFoodItem(
                        keb, 3
                );
                CartFoodItem cartFood2 = new CartFoodItem(
                        jooj, 5
                );

                Database.cartFoodItemRepository.add(cartFood1, userId);
                Database.cartFoodItemRepository.add(cartFood2, userId);
                String restaurantId = "6";
                Restaurant restaurant = new Restaurant(
                    restaurantId,
                    "لقمه بازار",
                    new Location(3,4),
                    new URL("https://google.com")
                );
                Database.restaurantRepository.addRestaurant(restaurant);
                Order order = new Order(restaurantId);
                order.setEta(Time.estimateEta(user.getLocation(), restaurant.getLocation()));
                int orderId = Database.orderRepository.add(order, userId);
                Database.cartFoodItemRepository.updateByOrderId(userId, orderId);
                ArrayList<Order> foundOrders = Database.orderRepository.findByUserId(userId);
                //assertEquals(foundOrders.get(0), order);

    }
}
