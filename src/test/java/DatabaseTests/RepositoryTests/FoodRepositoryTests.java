package DatabaseTests.RepositoryTests;


import Model.Food;
import Model.Location;
import Model.Restaurant;
import Repository.jdbc.Database;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FoodRepositoryTests {

    @Test
    public void addAndFindTest(){
        Food food = new Food(
            "1",
            "kebab",
            "kebab.jpg",
            "very delicious kebab",
            1f,
            2000,
            "1");

        Food food2 = new Food(
                "2",
                "jooje",
                "jooje.jpg",
                "very delicious jooje",
                1f,
                3000,
                "1");

        Restaurant rest = null;
        try {
            rest = new Restaurant(
                "1",
                    "bonab",
                new Location(1, 2),
                    new URL("https://google.com")
            );
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Database.restaurantRepository.addRestaurant(rest);
        Database.foodRepository.add(food);
        Database.foodRepository.add(food2);
        ArrayList<Food> foundFoods = Database.foodRepository.findByRestaurantId("1");
        assertEquals(foundFoods.get(0),food);
        assertEquals(foundFoods.get(1),food2);

    }

    @Test
    public void searchByNameTests(){
        Food food = new Food(
                "1",
                "kebab",
                "kebab.jpg",
                "very delicious kebab",
                1f,
                2000,
                "1");

        Food food2 = new Food(
                "2",
                "جوجه کباب",
                "jooje.jpg",
                "very delicious jooje",
                1f,
                3000,
                "1");

        Restaurant rest = null;
        try {
            rest = new Restaurant(
                    "1",
                    "bonab",
                    new Location(1, 2),
                    new URL("https://google.com")
            );
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Database.restaurantRepository.addRestaurant(rest);
        Database.foodRepository.add(food);
        Database.foodRepository.add(food2);
        ArrayList<String> foundFoods = Database.foodRepository.searchRestaurantIdsByFoodName("جوجه");
        assertThat(foundFoods, hasItems("1"));
    }
}
