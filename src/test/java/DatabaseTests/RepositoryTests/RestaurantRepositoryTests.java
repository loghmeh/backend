package DatabaseTests.RepositoryTests;

import Model.Location;
import Model.Restaurant;
import Model.User;
import Repository.jdbc.Database;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

public class RestaurantRepositoryTests {
    @Test
    public void addAndFindTest() {
        Restaurant restaurant;
        try {
            restaurant = new Restaurant(
                    "1",
                    "اکبر جوجه",
                    new Location(1, 1),
                    new URL("https://google.com")
            );
        } catch (MalformedURLException e) {
            restaurant = null;
        }

        Database.restaurantRepository.addRestaurant(restaurant);
        Restaurant foundRestaurant = Database.restaurantRepository.findById("1");
        System.out.println(foundRestaurant);
        assertEquals(restaurant, foundRestaurant);
    }

    @Test
    public void addAndExistsTest() {
        Restaurant restaurant;
        try {
            restaurant = new Restaurant(
                    "5",
                    "اکبر جوجه",
                    new Location(1, 1),
                    new URL("https://google.com")
            );
        } catch (MalformedURLException e) {
            System.out.println("Rid");
            restaurant = null;
        }

        Database.restaurantRepository.addRestaurant(restaurant);
        boolean found = Database.restaurantRepository.restaurantExists(restaurant.getId());
        assertTrue(found);
    }


    @Test
    public void addAndFindByIdAndFindAllTest() {
        Restaurant r1 = null, r2 = null;
        try {
            r1 = new Restaurant(
                    "5",
                    "اکبر جوجه",
                    new Location(1, 1),
                    new URL("https://google.com")
            );
            r2 = new Restaurant(
                    "6",
                    "اصغر جوجه",
                    new Location(1, 1),
                    new URL("https://google.com")
            );
            Database.restaurantRepository.addRestaurant(r1);
            Database.restaurantRepository.addRestaurant(r2);
        } catch (MalformedURLException e) {
            System.out.println("Rid");
        }

        ArrayList<Restaurant> restaurants = Database.restaurantRepository.findAll();
        assertThat(restaurants, hasItems(r1, r2));

        Restaurant foundRestaurant = Database.restaurantRepository.findById("6");
        assertEquals(r2, foundRestaurant);
    }

    @Test
    public void searchRestaurantNameTest() {
        Restaurant r1 = null;
        try {
            r1 = new Restaurant(
                    "10",
                    "ممد جوجه",
                    new Location(1, 1),
                    new URL("https://google.com")
            );

            Database.restaurantRepository.addRestaurant(r1);
        } catch (MalformedURLException e) {
            fail();
        }

        ArrayList<Restaurant> restaurants = Database.restaurantRepository.searchByName("ممد جوجه");
        assertThat(restaurants, hasItems(r1));
    }
}
