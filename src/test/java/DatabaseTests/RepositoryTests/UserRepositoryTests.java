package DatabaseTests.RepositoryTests;

import Model.Location;
import Model.User;
import Repository.jdbc.Database;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserRepositoryTests {
    @Test
    public void addAndFindTest() {
        User user = new User();
        user.setLocation(new Location(1, 2));
        user.setFirstName("Mammad");
        user.setLastName("Naseri");
        user.setEmail("mammad@naseri.loghmeh");
        user.setPhoneNumber("09396962772");
        user.setBalance(10000);

        int userId = Database.userRepository.addUser(user, "123456");
        user.setId(userId);
        User foundUser = Database.userRepository.findById(userId);
//        System.out.println(foundUser);
        assertEquals(user, foundUser);
    }

    @Test
    public void updateBalanceTest() {
        User user = new User();
        user.setLocation(new Location(1, 2));
        user.setFirstName("Mammad");
        user.setLastName("Naseri");
        user.setEmail("mammad@naseri.loghmeh");
        user.setPhoneNumber("09396962772");
        user.setBalance(10000);

        int userId = Database.userRepository.addUser(user, "123456");
        user.setId(userId);

        int chargeAmount = 10000;
        int newBalance = user.getBalance() + chargeAmount;
//        user.setBalance(newBalance);
        Database.userRepository.increaseBalance(userId, chargeAmount);

        User returnedUser = Database.userRepository.findById(userId);

        assertEquals(newBalance, returnedUser.getBalance());
    }
}
