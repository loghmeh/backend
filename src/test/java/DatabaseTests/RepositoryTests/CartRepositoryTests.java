package DatabaseTests.RepositoryTests;

import Model.Cart;
import Repository.jdbc.Database;
import org.junit.Test;

public class CartRepositoryTests {
    @Test
    public void createCartTest() {
        int id = 30;
        Database.cartRepository.createCart(id);
        Cart foundCart = Database.cartRepository.findById(id);
        assert foundCart != null;
    }
}
