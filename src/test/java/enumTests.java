import Model.PartyStatus;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class enumTests {


    @Test
    public void enumValueTest() {
        PartyStatus status = PartyStatus.ACTIVE;
        int statusValue = status.getValue();
        assertEquals(status, PartyStatus.of(statusValue));
    }
}
