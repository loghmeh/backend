FROM maven:3.6.1-jdk-8-alpine as BUILD
WORKDIR /build
COPY pom.xml /build/
RUN mvn --no-transfer-progress dependency:go-offline
COPY src/ /build/src/
RUN mvn --no-transfer-progress package

FROM tomcat:9.0.31-jdk8-openjdk-slim
COPY --from=BUILD /build/target/ROOT.war /usr/local/tomcat/webapps/
